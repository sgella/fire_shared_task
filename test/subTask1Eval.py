
# subTask1Eval.py
# Python code to evaluate subtask 1 of Transliteration Track at FIRE 2013

# Author: Rishiraj Saha Roy
# Date created: 08 Oct 2013
# Date modified: 09 Oct 2013

import sys

def main():
	# print len(sys.argv)
	if len(sys.argv) != 4:
		print "Usage: python subTask2Eval.py <your-output.txt> <reference-annotations.txt> <lang-id>"
		# lang-id = 1 for Hindi, 2 for Bangla, 3 for Gujarati
		exit()
	f1 = open(sys.argv[1], 'r')	# open output, ranked lists without RJs
	f2 = open(sys.argv[2], 'r')	# open reference qrels
	hypList = f1.readlines()	# read in lines of output/hypothesis
	refList = f2.readlines()	# read in lines of reference qrels	
	f1.close()
	f2.close()	
	
	# print hypList # prints garbled text on console due to unicode
	# print refList # prints garbled text on console due to unicode
	
	lc = len(hypList)
	# print "Line count = " + str(lc)
	if len(hypList) != len(refList):
		print 'Line count mismatch in input files. Exiting program.'
		exit()
	
	f3 = open('subTask1Results.txt', 'w')
	
	engPrec = 0.0
	engRec = 0.0
	engF = 0.0
	langPrec = 0.0
	langRec = 0.0
	langF = 0.0
	
	exactMatches = 0
	
	countEE = 0
	countEL = 0
	countLE = 0
	countLL = 0
		
	for i in range(0, lc):
		hypEntry = hypList[i]
		# print hypEntry
		hypEntry = hypEntry[:-1]	# strip the newline
		# print hypEntry
		hypParts = hypEntry.split()
		print hypParts
		refEntry = refList[i]
		refEntry = refEntry[:-1]	# strip the newline
		refParts = refEntry.split()
		print refParts
		if hypEntry == refEntry:
			exactMatches = exactMatches + 1
		numWords = len(refParts)
		#print numWords, len(hypParts)
		for j in range(0, numWords):
			print j, hypParts[j]
			hypParts2 = hypParts[j].split('\\')
			# print str(hypParts2) + "Len hyp parts sec " + str(len(hypParts2))
			refParts2 = refParts[j].split('\\')
			# print str(refParts2) + "Len ref parts sec " + str(len(refParts2))
			
			hypCheck = hypParts2[1] # part to check - annotation
			refCheck = refParts2[1] # part to check - annotation
			
			concat = hypCheck[0] + refCheck[0]
			# print concat # tag string for evaluation
			
			# "Lang-id = " + sys.argv[3]
			if int(sys.argv[3]) == 1:
				concat = concat.replace('H', 'L')
			elif int(sys.argv[3]) == 2:
				concat = concat.replace('B', 'L')
			elif int(sys.argv[3]) == 3:
				concat = concat.replace('G', 'L')
			else:
				print "Invalid language id. Exiting program."
				exit()
			
			# print concat # tag string for evaluation
			
			if concat == 'EE':
				countEE = countEE + 1
			elif concat == 'EL':
				countEL = countEL + 1
			elif concat == 'LE':
				countLE = countLE + 1
			elif concat == 'LL':
				countLL = countLL + 1
			
	exactMatches = float(exactMatches) / float(lc)
	
	# print countEE
	# print countEL	
	# print countLE
	# print countLL
	
	countEE = float(countEE)
	countEL = float(countEL)
	countLE = float(countLE)
	countLL = float(countLL)
	
	# print countEE
	# print countEL
	# print countLE
	# print countLL
	
	engPrec = countEE / (countEE + countEL)
	engRec = countEE / (countEE + countLE)
	engF = (2.0 * engPrec * engRec)/(engPrec + engRec)
	
	langPrec = countLL / (countLL + countLE)
	langRec = countLL / (countLL + countEL)
	langF = (2.0 * langPrec * langRec)/(langPrec + langRec)
	
	f3.write("Exact match fraction: " + str(exactMatches) + '\n\n')
	
	f3.write("English precision: " + str(engPrec) + '\n')
	f3.write("English recall: " + str(engRec) + '\n')
	f3.write("English F-score: " + str(engF) + '\n\n')
	
	f3.write("Language precision: " + str(langPrec) + '\n')
	f3.write("Language recall: " + str(langRec) + '\n')
	f3.write("Language F-score: " + str(langF) + '\n\n')
	
	f3.close()
		
if __name__ == "__main__":
	main()
	
# end program
