#!/usr/bin/Rscript

args <- commandArgs(TRUE)
input_file = args[1]
graph_file = args[2]

#data = read.csv("hindi_results.rdata", header = F, sep = " ")
data = read.csv(input_file, header = F, sep = " ")

xlbls = t(data[1,])
naive = t(data[2,])
maxent = t(data[3,])
decision = t(data[4,])
#adverb = t(data[5,])
#adjective = t(data[6,])

#y_range = range(0, ori, exp, randSmall, randBig)
y_range = range(20, 100)
#y_range = range(0, ori)

pdf(graph_file, width = 6, height = 4)
#pdf("../graphs/hindi_results.pdf", width = 6, height = 4)
#par(oma = c(0, 0, 0, 0), mar = c(4, 3, 1, 1))
#par( oma = c(0, 0, 0, 0), mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)
par( oma = c(0, 0, 0, 0), mar=c(4, 3, 1,8), xpd=TRUE)
plot(naive, type="l", col = "black", ylim = y_range, axes = F, ann = F, lty=1)
lines(maxent, col = "red",  lwd=1, lty=2)
lines(decision, col = "green", lwd=1, lty=4)
#lines(adjective,  col = "blue", lwd=1, lty=5)
#lines(adverb, col = "magenta", lwd=1, lty=6)
axis(1, at = seq(length(xlbls)), lab = xlbls)
title(xlab = "d")
axis(2, mgp=c(3, .5, 0))
title(ylab = "accuracy %", line = 1.8)
box()
legend("bottomleft", inset=c(1,0),c("NaiveBayes","Max-Ent","Decision-Tree"),bty="n", lwd=c(1,1,1),col=c("black","red","green"),lty=c(1,2,4))
dev.off()
