import os



def get_word_form(english_lexicon, word):
        if english_lexicon.has_key(word):
                return word
        elif english_lexicon.has_key(word.lower()):
                return word.lower()
        else:
                return None


def confirm_check(classx, scorex, english_lexicon, word, class2):
        wordx = get_word_form(english_lexicon, word)
        if int(scorex) == 1 and classx == "english" and  (wordx== None or english_lexicon[wordx]<20):
                 classx = class2
        elif classx =="english" and ( wordx == None or english_lexicon[wordx] < 20):
                classx = class2
	elif classx == "english" and wordx!=None and len(wordx) == 2 and english_lexicon[wordx] < 50:
                classx = class2
        return classx, scorex



english_lexicon = {}
for line in open(os.path.join("../data/", "extra_resources","english", "word_frequencies.txt")):
        spl = line.strip().split()
        english_lexicon[spl[0]] = int(spl[1])

print "filename,language, match, total, accuracy"
for language in ['hindi', 'bangla', 'gujarati']:
#for language in ['hindi']:
	for size in ['100', '200', '500', '1000', '2000', '3000', '5000']:
	#for size in ['1000']:
		#for algo in ['naive']:
		for algo in ['naive', 'maxent', 'decision']:
			total = 0.0
			match = 0.0
			cmatrix = [[0.0, 0.0], [0.0, 0.0]]
			i = -1
			actuals = []
			words = []
			for line in open("../mappings/test_"+language+"_"+size+".txt"):
				if line.strip()=="":
					actuals.append('SENT')
					words.append('SENT')
				else:
					actuals.append(line.split()[0])
					words.append(line.split()[1])
			#actuals = [line.split()[0] for line in open("../mappings/test_"+language+"_"+size+".txt") if line.strip()!=""]
			#print actuals
			for line in open(os.path.join("../predictions", language+"_"+size+".predictions."+algo)):
				i+=1
				#print i, os.path.join("../predictions", language+"_"+size+".predictions."+algo), len(actuals)
				[x, class1, score1, class2, score2] = line.strip().split()				
				actual = actuals[i]
				word = words[i]
				if actual=='SENT':
					i+=1
					actual = actuals[i]
					word = words[i]
				if float(score1) >= float(score2):
					class1, score1 = confirm_check(class1, float(score1), english_lexicon, word, class2)
					score = score1
					mclass = class1
				elif float(score1) < float(score2):
					class2, score2 = confirm_check(class2, float(score2), english_lexicon, word, class1)
					score = score2
					mclass = class2	
				else:
					print os.path.join("../predictions", language+"_"+size+".predictions."+algo), score1, score2	
				if mclass == actual:
					match+=1

				if mclass == language and actual == language:
					cmatrix[0][0]+=1
				elif mclass != language and actual != language:
					cmatrix[1][1]+=1
				elif mclass == language and actual != language:
					cmatrix[1][0]+=1
				elif mclass != language and actual == language:
					cmatrix[0][1]+=1
				else:
					print os.path.join("../predictions", language+"_"+size+".predictions."+algo), mclass, actual

				total+=1
			print os.path.join("../predictions", language+"_"+size+".predictions."+algo),",", language, ",", match,",",total,",", match/total
				
