import os
import csv
import re

def is_ascii(s):
    return all(ord(c) < 128 for c in s)

def load_transliterations(language):
	t_map = {}
	for line in open(os.path.join("../data", "extra_resources", language, "full_transliterations.txt"), 'r'):
		spl = line.strip().split()
		if len(spl)>1 and re.match("^[A-Za-z]*$", spl[0]):
			if not t_map.has_key(spl[0]):
				t_map[spl[0]] = {}
			if not t_map[spl[0]].has_key(spl[1]):
				t_map[spl[0]][spl[1]] = 0
			t_map[spl[0]][spl[1]] +=1
	return t_map


def get_translit(word, trans_map):
	if trans_map.has_key(word):
		return sorted(trans_map[word].items(), key = lambda x:x[1], reverse=True)[0][0]
	elif trans_map.has_key(word.lower()):
		return sorted(trans_map[word.lower()].items(), key = lambda x:x[1], reverse=True)[0][0]
	else:
		return ""

def get_word_form(english_lexicon, word):
	if english_lexicon.has_key(word):
		return word
	elif english_lexicon.has_key(word.lower()):
		return word.lower()
	else:
		return None
			
def get_context_row(predicted_row, word, prob, prev, borrowed, english_lexicon):
	row = [word]
	[x, class1, score1, class2, score2] = predicted_row
	#score1 = float(score1)
	#score2 = float(score2)
	if prev!=None:
		if prev == class1:
			score1 = score1* prob
		else:
			score1 = score1*(1-prob)
		if prev == class2:
			score2 = score2 * prob
		else:
			score2 = score2*(1-prob)
	if (not re.match("^[A-Za-z]*$", word)) and is_ascii(word):
		row.extend(['E', 1.0])
		mclass = 'english'
	elif word.isupper() or borrowed:
		row.extend(['E', 1.0])
		mclass = 'english'
	else:
		wordx = get_word_form(english_lexicon, word)
		if score1 >= score2:
			score = score1
  		        mclass = class1
			if mclass == "english" and (wordx==None or english_lexicon[wordx] <20):
				#print "something fishy", word, word.lower()
				mclass = class2
		elif score1 < score2:
			score = score2
	                mclass = class2
			if mclass == "english" and (wordx==None or english_lexicon[wordx] <20): 
				#print "something fishy", word, word.lower()
				mclass = class1
		row.extend([mclass[0].title(), score])
	#still need to append translit
	return row, mclass


def confirm_check(classx, scorex, english_lexicon, word, class2):
	wordx  = get_word_form(english_lexicon, word)
	if int(scorex) == 1 and classx == "english" and  (wordx == None or english_lexicon[wordx]<20):
		 classx = class2
	elif classx == "english" and (wordx == None or english_lexicon[wordx]<20 ):
		classx = class2
	elif classx == "english" and wordx!=None and len(wordx) ==2 and english_lexicon[wordx] < 50:
		classx = class2
	return classx, scorex

if __name__ == '__main__':
	english_lexicon = {}
	for line in open(os.path.join("../data/", "extra_resources","english", "word_frequencies.txt")):
		spl = line.strip().split()
		english_lexicon[spl[0]] = int(spl[1])
	for language in  ['hindi', 'gujarati', 'bangla']:
		trans_map = load_transliterations(language)			
		for size in ['100', '200', '500', '1000', '2000', '3000', '5000']:
			words = []
			for line in open (os.path.join("../mappings", "real_test_"+language+"_"+size+".txt"), 'r'):
                                if line.strip()=="":
                                        words.append('SENT')
                                else:
                                        words.append(line.split()[1])
			for algo in ['naive', 'maxent', 'decision']:
				predicted_rows = [line.strip().split() for line in open(os.path.join("../predictions", "real_"+language+"_"+size+".predictions."+algo))]	
				writer1 = csv.writer(open(os.path.join("../csv_predictions", "real_"+language+"_"+size+".predictions."+algo), 'w'), delimiter=";")
				row = ['word', 'label', 'match_score', 'translit']
				writer1.writerow(row)
				prev = None
				context_row_data = {}
				prev_values = {'0.6':None, '0.65':None, '0.7':None, '0.75': None, '0.8':None, '0.85':None, '0.9':None, '0.95':None}
				i = -1
				for predicted_row in predicted_rows:
					i+=1
					word = words[i]
                                        while word=='SENT':
						writer1.writerow(["", "","",""])
						for prob in ['0.6', '0.65', '0.7', '0.75', '0.8', '0.85', '0.9', '0.95']:
							context_row_data[prob].append(["","","",""])
							prev_values[prob] = None
        	                                i+=1
                	                        word = words[i]
                        	                prev = None
					translit = get_translit(word, trans_map)
					borrowed = False
					if "**" in translit:
						if word in english_lexicon and english_lexicon[word]>100:
							borrowed = True
							#print "borrowed", word, english_lexicon[word]
						translit = translit.replace('*','')
					
					row = [word]
					[x, class1, score1, class2, score2] = predicted_row
					score1 = float(score1)
					score2 = float(score2)
					predicted_row = [x, class1, score1, class2, score2]
					#fill normal row and write ro writer1					
					if (not re.match("^[A-Za-z]*$", word)) and is_ascii(word):
				                row.extend(['E', 1.0, translit])
				        elif word.isupper() or borrowed:
				                row.extend(['E', 1.0, translit])
					else:
						if score1> score2:
							check_class1, check_score1 = confirm_check(class1, score1, english_lexicon, word, class2)
							row.extend([check_class1[0].title(), check_score1, translit])
						else:
							check_class2, check_score2 = confirm_check(class2, score2, english_lexicon, word, class1)
							row.extend([check_class2[0].title(), check_score2, translit])
					writer1.writerow(row)
					#fill context rows data
					for prob in ['0.6', '0.65', '0.7', '0.75', '0.8', '0.85', '0.9', '0.95']:
						if not context_row_data.has_key(prob):
							context_row_data[prob] = [['word', 'label', 'match_score', 'translit']]
						temp, mclass = get_context_row(predicted_row, word, float(prob), prev_values[prob], borrowed, english_lexicon)
						#add translit
						temp.append(translit)						
	                                        prev_values[prob] = mclass
						context_row_data[prob].append(temp)

						
			
				for prob in ['0.6', '0.65', '0.7', '0.75', '0.8', '0.85', '0.9', '0.95']:
					writer2 = csv.writer(open(os.path.join("../csv_predictions", "real_"+language+"_"+size+".predictions."+algo+"_"+prob), 'w'), delimiter=";")
					for row in context_row_data[prob]:
						writer2.writerow(row)
		
		
