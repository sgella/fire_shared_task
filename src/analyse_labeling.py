import os


def getLabels(line, filename, orig):
	word_labels = []
	words_label_translit = line.split()
	for word_label_item in words_label_translit:
		if 'aadambarG=' in word_label_item:
			word_label_item = word_label_item.replace("aadambarG", "aadambar")
		#print word_label_item, filename
		word, language = word_label_item.split('=')[0].split("\\")
		word_labels.append((word, language, orig))
	return word_labels	

def findNonMatchCases(submitted_anno, reference_anno, submitted_file, annotated_file, language, run):
	total = 0
	match = 0
	for (word1, pref_label, orig1), (word2, ref_label, orig2) in zip(submitted_anno, reference_anno):
		if pref_label!= ref_label and ref_label!='N':
			print language+","+run+","+word1+"\\"+pref_label+","+ word2+"\\"+ref_label+","+orig1
			#print (word1, pref_label), (word2, ref_label)

def evaluate(submitted_file, annotated_file, original_test_file, language, run):
	submitted = [line.strip() for line in open(submitted_file, 'r'). readlines()]
	annotated = [line.strip() for line in open(annotated_file, 'r'). readlines()]
	original = [line.strip() for line in open(original_test_file, 'r').readlines()]
	submitted_anno = []
	reference_anno = []
	#print submitted_file, annotated_file
	for pred, ref, orig in zip(submitted, annotated, original):
		pred_anno = getLabels(pred, submitted_file, orig)
		ref_anno = getLabels(ref, annotated_file, orig)
		if len(ref_anno)!= len(pred_anno):
			#print submitted_file, annotated_file, len(pred_anno), len(ref_anno), pred_anno, ref_anno
			#print pred, ref, orig
			#print language, orig, annotated_file
			case = "exception"
		else:
			submitted_anno.extend(pred_anno)
			reference_anno.extend(ref_anno)
	findNonMatchCases(submitted_anno, reference_anno, submitted_file, annotated_file, language, run)					
	#print len(submitted), len(annotated)

if __name__ == '__main__':

	files = {}
	import re

	for filename in os.listdir("../SubTask1_Spandana_Jatin_submission2"):
		matchObj = re.match(r'subtask1run(.*)_(.*).txt', filename)
		run = matchObj.group(1)
		language = matchObj.group(2).title()
		if not files.has_key(language):
			files[language] = []
		files[language].append((os.path.join("../SubTask1_Spandana_Jatin_submission2",filename), "run"+run))

	for language in files:
		for filename in os.listdir("../Subtask1-test-with-anno"):
			if language in filename:
				for (filename1, run) in files[language]:
					evaluate(filename1, os.path.join("../Subtask1-test-with-anno",filename), os.path.join("../test",language+"_Test.txt"), language, run)
