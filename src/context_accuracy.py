import os


def get_word_form(english_lexicon, word):
        if english_lexicon.has_key(word):
                return word
        elif english_lexicon.has_key(word.lower()):
                return word.lower()
        else:
                return None


def confirm_check(classx, scorex, english_lexicon, word, class2):
	wordx = get_word_form(english_lexicon, word)
        if int(scorex) == 1 and classx == "english" and  (wordx== None or english_lexicon[wordx]<20):
                 classx = class2
	elif classx =="english" and ( wordx == None or english_lexicon[wordx] < 20):
		classx = class2
	elif classx == "english" and wordx!=None and len(wordx) == 2 and english_lexicon[wordx] < 50:
                classx = class2
        return classx, scorex


from optparse import OptionParser
opts = OptionParser()
opts.add_option("-p", "--probability", dest="probability", type = "float", default="0.6", help="By default total words to be considered is set to 1000")
#opts.add_option("-w", "--words", dest="words", default="frequency", help="By default we select top N freq words in transliterated text, another options is choosing randomly")
#opts.add_option("-m","--mallet_files", dest="mallet_files", default="../mallet_files", help="path for default directory to mallet")
#opts.add_option("-v", "--cross_validation", dest = "cross_validation", type = "int", default = "20", help ="defines the number of runs for cross validation, by default it is set to 10")
options, args = opts.parse_args()

print "filename, probability, language, match, total, accuracy"

english_lexicon = {}
for line in open(os.path.join("../data/", "extra_resources","english", "word_frequencies.txt")):
	spl = line.strip().split()
        english_lexicon[spl[0]] = int(spl[1])

for prob in [0.4,0.45,0.5,0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]:
	options.probability = prob
	for language in ['hindi', 'bangla', 'gujarati']:
		for size in ['100', '200', '500', '1000', '2000', '3000', '5000']:
			actuals = []
			words = []
			for line in open("../mappings/test_"+language+"_"+size+".txt"):
				if line.strip()=="":
					actuals.append('SENT')	
					words.append('SENT')			
				else:
					actuals.append(line.split()[0])
					#print line
					words.append(line.split()[1])
			for algo in ['naive', 'maxent', 'decision']:
				total = 0.0
				match = 0.0
				cmatrix = [[0.0, 0.0], [0.0, 0.0]]
				i = -1
				#actuals = [line.split()[0] for line in open("../mappings/test_"+language+"_"+size+".txt") if line.strip()!=""]
				#print actuals
				prev = None
				for line in open(os.path.join("../predictions", language+"_"+size+".predictions."+algo)):
					i+=1
					#print i, os.path.join("../predictions", language+"_"+size+".predictions."+algo), len(actuals)
					[x, class1, score1, class2, score2] = line.strip().split()				
					actual = actuals[i]
					word = words[i]
					score1 = float(score1)
					score2 = float(score2)
					if actual=='SENT':
						i+=1
						actual = actuals[i]
						word = words[i]
						prev = None
					if prev!=None:
						if prev == class1:
							score1 = score1* options.probability
						else:
							score1 = score1*(1-options.probability)
						if prev == class2:
							score2 = score2 * options.probability
						else:
							score2 = score2*(1-options.probability)

					if score1 >= score2:
						class1, score1  = confirm_check(class1, score1, english_lexicon, word, class2)
						mclass = class1
						score = score1
					elif score1 < score2:
						class2, score2  = confirm_check(class2, score2, english_lexicon, word, class1)
						score = score2
						mclass = class2	
					else:
						print os.path.join("../predictions", language+"_"+size+".predictions."+algo), score1, score2	
					if mclass == actual:
						match+=1
					prev = mclass

					if mclass == language and actual == language:
						cmatrix[0][0]+=1
					elif mclass != language and actual != language:
						cmatrix[1][1]+=1
					elif mclass == language and actual != language:
						cmatrix[1][0]+=1
					elif mclass != language and actual == language:
						cmatrix[0][1]+=1
					else:
						print os.path.join("../predictions", language+"_"+size+".predictions."+algo), mclass, actual

					total+=1
				print os.path.join("../predictions", language+"_"+size+".predictions."+algo),",", prob,",", language,",", match,",", total ,",", match/total
				
