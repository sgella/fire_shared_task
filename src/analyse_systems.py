

def getPredictedAndReference(line):
	import re
	import unicodedata as ud

	matchObj = re.match( r'Word: (.*); Your transliteration: (.*); Reference transliteration: (.*); .*', line)
	

	if matchObj:
		predicted = ud.normalize('NFC', matchObj.group(2).decode('utf8'))
		reference = ud.normalize('NFC', matchObj.group(3).decode('utf8'))
		#predicted = matchObj.group(2)
		#reference = matchObj.group(3)
		if predicted == reference:
			print matchObj.group(1),",", matchObj.group(2),",", matchObj.group(3)
			return True, 0
		elif len(predicted) >= len(reference):# and matra in matchObj.group(2):
			return False, 1
		elif len(predicted) < len(reference):# and  matra in matchObj.group(1):
			return False, -1
	else:
		return False, -10
	


def getWordsAndDetails(data):
	words = {}
	matched = 0
	pred = 0
	ref = 0
	eql = 0
	for line in data:
		match, val = getPredictedAndReference(line)
		if match:
			matched+=1
		if val ==1:
			pred+=1
		if val ==-1:
			ref+=1	
		if val == 0:
			eql +=1
		spl = line.split(";")
		spl2 = spl[0].split(': ')
		if not words.has_key(spl[1]):
			words[spl[1]] = 0
		words[spl[1]]+=1
	return words, matched, pred, ref, eql

if __name__ == '__main__':

    for language in ['Hin', 'Guj', 'Bang']:
	for system1 in ['1', '2','3']:
		for system2 in ['1', '2','3']:
			if system1!=system2 and int(system1) > int(system2):
				for typex, count in [('Incorrect', 10)]:#, ('Correct', 10)]:
					data1 = open('../MSRIRunsErrorAnalysis/MSRI-'+system1+'ST1'+language+'Gen'+typex+'TranslitWordPairs.txt', 'r').readlines()
					data2 = open('../MSRIRunsErrorAnalysis/MSRI-'+system2+'ST1'+language+'Gen'+typex+'TranslitWordPairs.txt', 'r').readlines()
					words1, matched1, pred1, ref1, eql1 = getWordsAndDetails(data1)			
					words2, matched2, pred2, ref2, eql2  = getWordsAndDetails(data2)
					commonWords = set(words1.keys()).intersection(set(words2.keys()))
					difference = set(words1.keys()).symmetric_difference(set(words2.keys()))
					#print language, "run"+system1, "run"+system2, typex, len(words1), len(words2), len(commonWords), len(difference), pred1, ref1, eql1, pred2, ref2, eql2
					print language, "run"+system1, "run"+system2, typex, pred1, ref1, pred2, ref2, eql1, eql2
					#print typex, sorted(words1.items(), key=lambda x:x[1], reverse=True)[:10]
					#print typex, sorted(words2.items(), key=lambda x:x[1], reverse=True)[:10]
					
