from nltk import ngrams
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import WordPunctTokenizer
from nltk.collocations import BigramCollocationFinder
from nltk.metrics import BigramAssocMeasures
from nltk.classify import NaiveBayesClassifier #, ConditionalExponentialClassifier, WekaClassifier, DecisionTreeClassifier
from nltk.classify.maxent import MaxentClassifier
from nltk.classify.util import accuracy
import sys
import urllib2
import os
import random

def extract_features(word, translated_pairs, flag):
    '''
    here we are extracting features to use in our classifier. We want to pull all the 1,2,3,4,5 character grams 
    in our input along with the word
    '''
    word = word.lower()
    features = []
    features = features + map("".join, ngrams(word, 1)) # uni
    features = features + map("".join, ngrams(word, 2)) # bi
    features = features + map("".join, ngrams(word, 3)) # tri
    features = features + map("".join, ngrams(word, 4)) # quad
    features = features + map("".join, ngrams(word, 5)) #penta
    features = features + [word]
    #if flag:
    #	features = features+['ENGLISH']
    #else:
    #	features = features+['OTHER']
    #features = features + [flag]
    #if word in translated_pairs:
    #	features = features + ['TRANSLIT']
    #else:
    #	features = features + ['NO_TRANSLIT']
    return features


def get_feature(word):
    return dict([(word, True)])


def bag_of_words(words):
    return dict([(word, True) for word in words])


def create_training_dict(text, sense, translated_pairs, flag):
    ''' returns a dict ready for a classifier's test method '''
    tokens = extract_features(text, translated_pairs, flag)
    return [(bag_of_words(tokens), sense)]

def load_test_words(options):
	test_words = {}
	total = 0
	class_map = {'H':'hindi', 'E':'english', 'B':'bangla', 'G':'gujarati'}
	for line in open(os.path.join("../data/subtask_1_data/",options.language+"_annotated_dev.txt"),'r'):
	#for line in open(os.path.join("../test", options.language.title()+"_Test.txt")):
		for word in line.strip().split():
#print word
			total +=1
			try:
				spl = word.split("\\")
				check_word = spl[0]
				word_type = class_map[spl[1].split("=")[0]]
				#word_type = class_map[options.language[0].title()]
				if not test_words.has_key(word_type):
					test_words[word_type] = {}
				if check_word in english_words:
					test_words[word_type][check_word] = [1, '']
				else:
					test_words[word_type][check_word] = [0, '']
			except:
				pass
	#print "total words in text", total
        #fp = open("test-"+options.language+".txt", 'w')
	#count = 1
        #for language in test_words:
	#        for word in test_words[language]:
        #	        fp.write("instance"+str(count)+"\t"+language+"\t"+word+"\n")
        #        	count+=1
        #fp.close()

       
	return test_words

def run_classifier_tests(classifier, transliterated_pairs, english_lexicon, test_words, options):
    #testfiles = [{'fruit': 'http://litfuel.net/plush/files/disambiguation/apple-fruit-training.txt'},
    #             {'company': 'http://litfuel.net/plush/files/disambiguation/apple-company-training.txt'}]
    testfeats = []
    acc = 0.0
    total = 0
    fp = open("../mallet_input_files/test_"+options.language+".txt", 'w')
    fp1 = open("test-results-"+options.language+".txt", 'w')
    count = 1
    for class_type in test_words:
    	for word in test_words[class_type]:
		total+=1
		if word in english_words:
			#tokens = create_training_dict(word, class_type, transliterated_pairs, 'True')[0]
			features = extract_features(word, transliterated_pairs, 1)
			tokens = bag_of_words(features)
			flag = 1
		else:
			#tokens = create_training_dict(word, class_type, transliterated_pairs, 'False')[0]
			features = extract_features(word, transliterated_pairs, 0)
			tokens = bag_of_words(features)
			flag = 0
		#line="instance-test"+str(count)
		line = class_type
		for feature in features:
			line+=" "+str(feature)+":1"
		fp.write(line.strip()+"\n")
		fp1.write("instance-test"+str(count)+"\t"+class_type)
		count+=1
    		#testfeats = testfeats + create_training_dict(word, class_type, transliterated_pairs, flag)
		decision = classifier.classify(tokens)
		if decision == class_type:
			acc+=1
		test_words[class_type][word][1] = decision
    fp.close()
    fp1.close()

    #acc = accuracy(classifier, testfeats) * 100
    #acc =0
    #print 'accuracy: %.2f%%' % acc
    return (acc/total)*100
    #sys.exit()

def get_classifier(train_set, options):
    if options.classifier == "naive":
    	classifier = NaiveBayesClassifier.train(train_set)
    elif options.classifier == "decision":
    	classifier = DecisionTreeClassifier.train(train_set)
    elif options.classifier == "conditional":
	classifier = ConditionalExponentialClassifier.train(train_set)
    elif options.classifier == "maxent":
	classifier = MaxentClassifier.train(train_set)

    return classifier
     

def get_train_set(options, train_words, translated_pairs, english_words):

    # create our dict of training data
    #texts = {}
    #texts['english'] = '/home/siva/spandana/microsoft/fire_shared_task/FIRE_shared_task/data/extra_resources/english/word_frequencies.txt'
    #texts['hindi'] = '/home/siva/spandana/microsoft/fire_shared_task/FIRE_shared_task/data/extra_resources/hindi/transliteration_pairs.txt'

    #holds a dict of features for training our classifier
    train_set = []

    # loop through each item, grab the text, tokenize it and create a training feature with it
    #for sense, file in texts.iteritems():
    #    #print "training %s " % sense
    #    words = [line.strip().split()[0] for line in open(file, 'r').readlines()]
    #    print "training %s " % sense, len(words), options.size

    fp = open("../mallet_input_files/training_"+options.language+"_"+str(options.size)+".txt", 'w')
    count = 1
    #for language in train_words:
    #   for word in train_words[language]:
    #           fp.write("instance"+str(count)+"\t"+language+"\t"+word.lower()+"\n")
    #           count+=1
    #fp.close()

    for language in train_words:
	for word in train_words[language][:options.size]:
		#print word
		if word in english_words:
	        	features = extract_features(word, translated_pairs, 1)
		else:
	        	features = extract_features(word, translated_pairs, 0)
		#line="instance-train"+str(count)+" "+language
		line = language
		count+=1
		fmap = {}
		for feature in features:
			if not fmap.has_key(feature):
				fmap[feature] = 0
			fmap[feature]+=1
		for feature in fmap:	
			line+=" "+str(feature)+":"+str(fmap[feature])
		fp.write(line+"\n")
        	train_set = train_set + [(get_feature(word), language) for word in features]
	#print train_set
    fp.close()
    

    import pickle
    #pickle.dump(train_set, open("train_set.pk", 'w')) 
    #classifier = NaiveBayesClassifier.train(train_set)
    classifier = get_classifier(train_set, options)
    #pickle.dump(classifier, open("classifier.pk", 'w')) 
    return classifier

    # uncomment out this line to see the most informative words the classifier will use
    #classifier.show_most_informative_features(20)


    # uncomment out this line to see how well our accuracy is using some hand curated tweets
    #run_classifier_tests(classifier)


    #for line in urllib2.urlopen("http://litfuel.net/plush/files/disambiguation/apple-tweets.txt", 'r'):

def get_training_words(language_map, options, stopwords):
    from operator import itemgetter

    train_words = {'english':[], options.language:[]}

    english_words = {} 
    for line in open(os.path.join('/home/siva/spandana/microsoft/fire_shared_task/FIRE_shared_task/data/extra_resources/', 'english', 'word_frequencies.txt'), 'r'):
    #for line in open(os.path.join('/home/siva/spandana/microsoft/fire_shared_task/FIRE_shared_task/data/extra_resources/', 'abner_word_frequencies.txt'), 'r'):
    #for line in open(os.path.join('/home/siva/spandana/microsoft/fire_shared_task/FIRE_shared_task/data/extra_resources/', 'colt_freq_list.txt'), 'r'):
    #for line in open(os.path.join('/home/siva/spandana/microsoft/fire_shared_task/FIRE_shared_task/data/sms', 'sms_message_corpus_word_frequencies.txt'), 'r'):
	spl = line.strip().split()
	english_words[spl[0]] = int(spl[1])

    
    #train_words['english'] = [key for key, value in sorted(english_words.items(), key=lambda x:x[1], reverse=True)[:options.size]] 
   
    #temp_list = random.sample( english_words.items(), options.size*10)
    if options.words == "random":
    	temp_list = random.sample( english_words.items(), options.size*5)
    	#temp_list = random.sample( english_words.items(), options.size*2)
    	#train_words['english'] = [key for key, value in sorted(temp_list,key=itemgetter(1), reverse=True) if key not in stopwords][:options.size]
    	train_words['english'] = [key for key, value in sorted(english_words.items(), key=lambda x:x[1], reverse=True)][:options.size]
    else:
	train_words['english'] = [key for key, value in sorted(english_words.items(), key=lambda x:x[1], reverse=True) if len(key)>=2 and key not in stopwords][:options.size]

    word_frequencies = {}
    for line in open(os.path.join('/home/siva/spandana/microsoft/fire_shared_task/FIRE_shared_task/data/extra_resources/', options.language, 'word_frequencies.txt'), 'r'):
	spl = line.strip().split()
	if len(spl)==2:
	    	word_frequencies[spl[0]] = int(spl[1])
	else:
		print spl
    
    #top words in original scirpt   
    if options.words == "frequency":
	top_words = [key for key, value in sorted(word_frequencies.items(), key=lambda x:x[1], reverse=True)] #[:options.size]]
    elif options.words =="random":
	temp_list = random.sample( word_frequencies.items(), options.size*20)
	top_words = [key for key, value in sorted(temp_list,key=itemgetter(1), reverse=True)]#[:options.size]

    count = 0
    if options.language == "bangla":
    	for word in top_words:
		if count<options.size:
			train_words[options.language].append(word)
			count+=1

    translated_pairs = {}
	
    added = []
    if options.language!="gujarati":
	    translated_file = open(os.path.join('/home/siva/spandana/microsoft/fire_shared_task/FIRE_shared_task/data/extra_resources/', options.language, 'transliteration_pairs.txt'), 'r').readlines()
    if options.language == "gujarati":
	    translated_file = open(os.path.join('/home/siva/spandana/microsoft/fire_shared_task/FIRE_shared_task/data/extra_resources/', options.language, 'full_transliterations.txt'), 'r').readlines()
    for line in translated_file:
	spl = line.strip().split()
	translated_pairs[spl[0]] = spl[1]
	if len(translated_file)>options.size and count<options.size:
		#print spl[0]
		if count<options.size and spl[1] in top_words and spl[1] not in added and options.language!="bangla":
			#print "added", spl[0], spl[1]
			train_words[options.language].append(spl[0])
			added.append(spl[1])
			count+=1
		if count>=options.size:
			break

	elif len(translated_file) <options.size and options.language!="bangla" and count<options.size:
		train_words[options.language].append(spl[0])
 
    print "english", len(train_words['english'])
    print options.language, len(train_words[options.language])
    #fp = open("training-"+options.language+".txt", 'w')
    #count = 1
    #for language in train_words:
    #	for word in train_words[language]:
    #		fp.write("instance"+str(count)+"\t"+language+"\t"+word.lower()+"\n")
    # 		count+=1
    #fp.close()

    return train_words, word_frequencies, translated_pairs, english_words

def calculate_micro_f(test_words):
	total = 0
	totale = 0
	totalh = 0
	confusion_matrix=[[0.0,0.0],[0.0,0.0]]
	for language in test_words:
		for word in test_words[language]:
			if language == options.language and test_words[language][word][1] == options.language:
				confusion_matrix[0][0]+=1
				totalh+=1
			elif language == "english" and test_words[language][word][1] == "english":
				confusion_matrix[1][1]+=1
				totale+=1
			elif language == options.language and test_words[language][word][1] =="english":
				confusion_matrix[0][1]+=1
				totalh+=1
			elif language == "english" and test_words[language][word][1] == options.language:
				confusion_matrix[1][0]+=1
				totale+=1

	#print total, confusion_matrix, totale, totalh
	ph = confusion_matrix[0][0]/(confusion_matrix[0][0] + confusion_matrix[1][0])
	rh = confusion_matrix[0][0]/(confusion_matrix[0][0] + confusion_matrix[0][1])

	pe = confusion_matrix[1][1]/(confusion_matrix[1][1] + confusion_matrix[0][1])
	re = confusion_matrix[1][1]/(confusion_matrix[1][1] + confusion_matrix[1][0])

	mi_p =  (confusion_matrix[0][0]+confusion_matrix[1][1])/(confusion_matrix[0][0] + confusion_matrix[1][0]+confusion_matrix[1][1] + confusion_matrix[0][1])
	mi_r =  (confusion_matrix[0][0]+confusion_matrix[1][1])/(confusion_matrix[0][0] + confusion_matrix[0][1]+confusion_matrix[1][1] + confusion_matrix[1][0])

	ma_p = (ph+pe)/2
	ma_r = (rh+re)/2

	print "ph rh pe re", ph, rh, pe, re
	
	#print options.language ,"precision recall fmeasure", ph , rh , (2*ph*rh)/(ph+rh), confusion_matrix[0][0] + confusion_matrix[1][0], confusion_matrix[0][0] + confusion_matrix[0][1]
	#print "english precision recall fmeasure", pe , re , (2*pe*re)/(pe+re), confusion_matrix[1][1] + confusion_matrix[0][1], confusion_matrix[1][1] + confusion_matrix[1][0]
	#print "micro avergae precision recall fmeasure", mi_p, mi_r, (2*mi_p*mi_r)/(mi_p+mi_r)

	#print "macro avergae precision recall fmreasure", ma_p, ma_r, (2*ma_p*ma_r)/(ma_p+ma_r)

	return (2*mi_p*mi_r)/(mi_p+mi_r)


#def generate_train_and_test_file()

if __name__ == '__main__':
   
    from optparse import OptionParser
    opts = OptionParser()
    opts.add_option("-d", "--data", dest="data", default="data", help="data directory path, by default its set to data directory")
    opts.add_option("-l", "--language", dest="language", default="hindi", help="other language other than english in bilingual word level prediction")
    opts.add_option("-c", "--classifier", dest="classifier", default="naive", help="type of classifier that should be used, by default its set to naive bayes")
    opts.add_option("-s", "--size", dest="size", type = "int", default="1000", help="By default total words to be considered is set to 1000")
    opts.add_option("-w", "--words", dest="words", default="random", help="By default we select top N freq words in transliterated text, another options is choosing randomly")
    opts.add_option("-v", "--cross_validation", dest = "cross_validation", type = "int", default = "20", help ="defines the number of runs for cross validation, by default it is set to 10")
    options, args = opts.parse_args()

    language_map= {'e':'english', 'h':'hindi', 'g':'gujarati', 'b':'bangla'}

    #sys.exit(0)

    import pickle
    #train_set = pickle.load(open("train_set.pk", 'r'))
    #classifier = NaiveBayesClassifier.train(train_set)

    #confusion_matrix=[[0.0,0.0],[0.0,0.0]]

    #total = 0
    #totale = 0
    #totalh = 0
    stopwords = [line.strip() for line in open("stopwords.txt")]
    if options.words == "frequency":
    	options.cross_validation  = 1

    f_measures = []
    accuracy = []
    for i in range(0, options.cross_validation):
    	train_words, word_frequencies, transliterated_pairs, english_words = get_training_words(language_map, options, stopwords)
	test_words = load_test_words(options)
	classifier = get_train_set(options, train_words, transliterated_pairs, english_words)
    	#classifier = pickle.load(open("classifier.pk", 'r')) 
	#accuracy.append(run_classifier_tests(classifier, transliterated_pairs, english_words, test_words, options))
	#f_measures.append(calculate_micro_f(test_words))
    #print f_measures
    #print "Average of cross validation", sum(f_measures)/len(f_measures)
    #print "Average of cross accuracy", sum(accuracy)/len(accuracy)

    f= open(os.path.join("../test", options.language.title()+"_Test_results.txt"), 'w')

    total = 0.0
    match = 0
    fp = open("../mallet_input_files/test_"+options.language+"_"+str(options.size)+".txt", 'w')
    #fp1 = open("../mallet_input_files/test_"+options.language+"_"+str(options.size)+".txt.us", 'w')
    fm = open("../mappings/test_"+options.language+"_"+str(options.size)+".txt", 'w')
    #ft = open("../data/extra_resources/"+ options.language+"/dev_transliterations.txt", 'w')
    
    #for line in open(os.path.join("../test", options.language.title()+"_Test.txt")):
    for line in open(os.path.join("../data/subtask_1_data/",options.language+"_annotated_dev.txt"),'r'):
	words = line.strip().split()
	result=""
	#print line
	for word in words:
		spl = word.split("\\")
		#if len(spl) == 1:
		if len(spl)>1:
			total+=1
			word = spl[0]
			#tag = options.language[0].title()
			tag = spl[1].split('=')[0]
			if tag!='E' and len(spl)>1:
				translit = spl[1].split('=')[1]
				#ft.write(word+"\t"+translit+"\n")
			class_map = {'H':'hindi', 'E':'english', 'B':'bangla', 'G':'gujarati'}
			language = class_map[tag]
			fm.write(language+"\t"+word+"\n")
			line = ""
			try:
				k = int(word)
				result+=word+"\E "		
				features = extract_features(word, transliterated_pairs, 0)
	                        tokens = bag_of_words(features)
			except:
				tword = word.lower()
				if word in english_words:
					features = extract_features(tword, transliterated_pairs, 1)
	                        	tokens = bag_of_words(features)
		                        flag = 1
        		        else:
                		        features = extract_features(tword, transliterated_pairs, 0)
                        		tokens = bag_of_words(features)
		                        flag = 0
			fmap = {}
                	for feature in features:
				if not fmap.has_key(feature):
					fmap[feature] = 0
				fmap[feature] +=1
			for feature in fmap:
                	      	line+=" "+str(feature)+":"+str(fmap[feature])
		        fp.write(language+" "+line.strip()+"\n")
		        #fp1.write(line.strip()+"\n")
        	
			decision = classifier.classify(tokens)
			if decision == language:
				match +=1
			if decision == options.language or word in transliterated_pairs or tword in transliterated_pairs:
				result+=word+"\\"+options.language[0].title()+"="
				if word in transliterated_pairs:
					result+=transliterated_pairs[word]+" "
				elif tword in transliterated_pairs:
					result+=transliterated_pairs[tword]+" "
				else:
					result+=" "
			else:
				result+=word+"\\E "
	f.write(result.strip()+"\n")
	fm.write("\n")
    f.close()
    #ft.close()
    fm.close()

    print "accuracy", match/total

    	
    #for line in open(os.path.join("../data/subtask_1_data/",options.language+"_annotated_dev.txt"),'r'):
    #    for word in line.strip().split():
    #		#print word
    #            total +=1
    #		try:
    #			spl = word.split("\\")
    #	                check_word = spl[0]
    #    	        word_type = spl[1].split("=")[0]
    #			if check_word in english_words:
    #				tokens = bag_of_words(extract_features(check_word, transliterated_pairs, 'True'))
    #			else:
    #				tokens = bag_of_words(extract_features(check_word, transliterated_pairs, 'False'))
    #			decision = classifier.classify(tokens)
    #			if word_type[0].lower() == options.language[0] and decision[0] == options.language[0].lower():
    #				confusion_matrix[0][0]+=1
    #				totalh+=1
    #			elif word_type[0].lower() == 'e' and decision[0] =='e':
    #				confusion_matrix[1][1]+=1
    #				totale+=1
    # 			elif word_type[0].lower() == options.language[0].lower() and decision[0] =='e':
    #				confusion_matrix[0][1]+=1
    #				totalh+=1
    #			elif word_type[0].lower() == 'e' and decision[0] == options.language[0].lower():
    #				confusion_matrix[1][0]+=1
    #				totale+=1
    #			else:
    #				print spl[0], word_type[0].lower(), decision[0]
    #		except:
    #			print "ERROR", prev
    #		prev = line
    #
    #print total, confusion_matrix, totale, totalh
    #ph = confusion_matrix[0][0]/(confusion_matrix[0][0] + confusion_matrix[1][0])
    #rh = confusion_matrix[0][0]/(confusion_matrix[0][0] + confusion_matrix[0][1])

    #pe = confusion_matrix[1][1]/(confusion_matrix[1][1] + confusion_matrix[0][1])
    #re = confusion_matrix[1][1]/(confusion_matrix[1][1] + confusion_matrix[1][0])

    #mi_p =  (confusion_matrix[0][0]+confusion_matrix[1][1])/(confusion_matrix[0][0] + confusion_matrix[1][0]+confusion_matrix[1][1] + confusion_matrix[0][1])
    #mi_r =  (confusion_matrix[0][0]+confusion_matrix[1][1])/(confusion_matrix[0][0] + confusion_matrix[0][1]+confusion_matrix[1][1] + confusion_matrix[1][0])

    #ma_p = (ph+pe)/2
    #ma_r = (rh+re)/2

    #print options.language ,"precision recall fmeasure", ph , rh , (2*ph*rh)/(ph+rh), confusion_matrix[0][0] + confusion_matrix[1][0], confusion_matrix[0][0] + confusion_matrix[0][1]
    #print "english precision recall fmeasure", pe , re , (2*pe*re)/(pe+re), confusion_matrix[1][1] + confusion_matrix[0][1], confusion_matrix[1][1] + confusion_matrix[1][0]
    #print "micro avergae precision recall fmeasure", mi_p, mi_r, (2*mi_p*mi_r)/(mi_p+mi_r)

    #print "macro avergae precision recall fmreasure", ma_p, ma_r, (2*ma_p*ma_r)/(ma_p+ma_r)
