import os
	
def get_classifier(options):
	
	#classifier_command = ../../mallet-2.0.7/bin/mallet train-classifier --input hindi-train.mallet --output-classifier hindi.classifier
	if options.classifier == "naive":
		trainer = " --trainer NaiveBayes"
	elif options.classifier == "maxent":
		trainer = " --trainer MaxEnt"
	elif options.classifier == "decision":
		trainer = " --trainer DecisionTree"
	else:
		trainer = " --trainer NaiveBayes --trainer MaxEnt --trainer DecisionTree"

	#classifier_command = "../../mallet-2.0.7/bin/mallet train-classifier --training-file "+os.path.join(options.mallet_files, options.language+"_"+str(options.size)+"_train.mallet")+" --testing-file "+os.path.join(options.mallet_files, options.language+"_"+str(options.size)+ "_test.mallet")+ trainer
	#print classifier_command
	#os.system(classifier_command)

	#create classifier

	command = "../../mallet-2.0.7/bin/mallet train-classifier --input "+os.path.join(options.mallet_files, options.language+"_"+str(options.size)+"_train.mallet")+" --output-classifier "+os.path.join("../classifiers", options.language+"_"+str(options.size)+".classifier"+"."+options.classifier) +trainer
	#print command
	os.system(command)

	
	

def classify_data(options):

	#import test/train ndata


        #../../mallet-2.0.7/bin/mallet import-svmlight --input training-hindi.txt test-hindi.txt --output hindi-train.mallet hindi-test.mallet
        #../../mallet-2.0.7/bin/mallet train-classifier --training-file hindi-train.mallet --testing-file hindi-test.mallet
        #../../mallet-2.0.7/bin/mallet train-classifier --training-file hindi-train.mallet --testing-file hindi-test.mallet --trainer maxEnt
        #../../mallet-2.0.7/bin/mallet train-classifier --training-file hindi-train.mallet --testing-file hindi-test.mallet --trainer MaxEnt


	train_import_command = "../../mallet-2.0.7/bin/mallet import-svmlight --input "+os.path.join("../mallet_input_files", "training_"+options.language+"_"+str(options.size)+".txt")+" "+os.path.join("../mallet_input_files", "test_"+options.language+"_"+str(options.size)+".txt")+" --output "+ os.path.join(options.mallet_files, options.language+"_"+str(options.size)+"_train.mallet")+ " "+os.path.join(options.mallet_files, options.language+"_"+str(options.size)+"_test.mallet")
	#print train_import_command
	os.system(train_import_command)

	
	#create classifier
	classifiers = []
	if options.classifier == "all":	
		classifiers = ['naive', 'maxent', 'decision']
	else:
		classifiers = [options.classifier]
		
	for classifier in classifiers:
		options.classifier = classifier
		get_classifier(options)
		print "stats", options.language, options.size, options.classifier
		#return
	
		#classify test file
		#command = "../../mallet-2.0.7/bin/mallet classify-file --input "+os.path.join("../mallet_input_files", "test_"+options.language+"_"+str(options.size)+ ".txt")+" --output "+ os.path.join("../predictions", options.language+"_"+str(options.size)+".predictions."+options.classifier)+" --classifier "+os.path.join("../classifiers", options.language+"_"+str(options.size)+".classifier"+"."+options.classifier)
		command = "../../mallet-2.0.7/bin/mallet classify-svmlight --input "+os.path.join("../mallet_input_files", "test_"+options.language+"_"+str(options.size)+ ".txt")+" --output "+ os.path.join("../predictions", options.language+"_"+str(options.size)+".predictions."+options.classifier)+" --classifier "+os.path.join("../classifiers", options.language+"_"+str(options.size)+".classifier"+"."+options.classifier)
		#print command
		os.system(command)

if __name__ == '__main__':

	from optparse import OptionParser
	opts = OptionParser()
	opts.add_option("-d", "--data", dest="data", default="data", help="data directory path, by default its set to data directory")
	opts.add_option("-l", "--language", dest="language", default="hindi", help="other language other than english in bilingual word level prediction")
	opts.add_option("-c", "--classifier", dest="classifier", default="naive", help="type of classifier that should be used, by default its set to naive bayes")
	opts.add_option("-s", "--size", dest="size", type = "int", default="1000", help="By default total words to be considered is set to 1000")
	opts.add_option("-w", "--words", dest="words", default="frequency", help="By default we select top N freq words in transliterated text, another options is choosing randomly")
	opts.add_option("-m","--mallet_files", dest="mallet_files", default="../mallet_files", help="path for default directory to mallet")
	opts.add_option("-v", "--cross_validation", dest = "cross_validation", type = "int", default = "20", help ="defines the number of runs for cross validation, by default it is set to 10")
	options, args = opts.parse_args()
	actual_classifier = options.classifier
	if options.size == -1:
		for size in [100, 200, 500, 1000, 2000, 3000, 5000]:
			options.size = size
			options.classifier  = actual_classifier
			classify_data(options)	
	else:
		classify_data(options)
