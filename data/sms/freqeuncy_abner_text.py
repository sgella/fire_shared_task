import re
chars = [',', '!', '.', ';', '?', '\"', '(', ')','-', ':', '/','#', '&', '|', '%']

word_dict = {}
for line in open("message_corpus.txt", 'r'):
	line = "".join(ch for ch in line if ord(ch)<128)
	line = re.sub('[%s]' % ''.join(chars), ' ', line.lower())
	spl = line.strip().split()
	for word in spl:
		try:
			k = int(word)
		except:
			if not word_dict.has_key(word):
				word_dict[word] = 0
			word_dict[word]+=1
fp = open("sms_message_corpus_word_frequencies.txt", 'w')
for word, freq in sorted(word_dict.items(), key=lambda x:x[1], reverse=True):
	fp.write("%s\t%d\n"%(word, freq))
fp.close()
	
	
