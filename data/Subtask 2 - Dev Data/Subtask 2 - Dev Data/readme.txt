qrels - Dev.txt: Relevance judgments containing query and document identifiers for the development set of 25 queries.

queryIndex - Dev.txt: An indexed list of the 25 development queries.

urlIndex.txt: The complete URL index for mapping provided documents in the collection to the actual URLs from which these were downloaded.
