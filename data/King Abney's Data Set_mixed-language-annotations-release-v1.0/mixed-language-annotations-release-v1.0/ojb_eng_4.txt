#CURRENT URL http://weshki.atwebpages.com/jones12.html
[[eng]] from Ojibwa Texts collected by William Jones (1919). 
[[ojb]] (1) Ningoding isa giiwenh gii-daa majiikiwis. 
[[eng]] Now, once on a time, they say, there dwelt a first-born son. 
[[ojb]] (2) Midaachiwan ogwisan, odaanisa' gaye midaachiwa'. 
[[eng]] Ten was the number of his sons, and his daughters were also ten. 
[[ojb]] (3) Mii idash ekidowaad igiw oskinaweg: “Aaniish, noose, mii isa zhigwa ji-bakewinigooyan. Bakaan akiing ningad-izhaamin”. 
[[eng]] Thereupon said the youths: "Well, my father, the time is now at hand for us to be leaving you. To a different land are we going." 
[[ojb]] (4) Mii dash geget gii-a-bagidinaad ogwisisa'; mii idash gii-maajaawaad. 
[[eng]] And so, in truth, he let his sons depart; whereupon they started away. 
[[ojb]] (5) Apii idash gaa-oditamowaad ge-daawaad mii iw gii-ozhitoowaad wiigiwaam. 
[[eng]] And when they came to the place where they were going to live, they then built a wigwam. 
[[ojb]] (6) Mii dash imaa wenjiiwaad andawenjigewaad; anooji gegoo onitoonaawaa ge-miijiwaad. 
[[eng]] And so from that place they set out when they went to hunt for game; all kinds of things they killed, what they were to eat. 
[[ojb]] (7) Mii idash ezhichigewaad: bezhigwanini omiikanaawa; nitam idash zeziikizid ani-bakemoni omiikana ezhaad nendawenjiged; miinawaa dash bezhig zeziikizid ani-bakemoni omiikana ezhaad nendawenjiged; mii go miinawaa eni-zaziikizid eni-bakemonik omiikana; mii go iw pane endoodamowaad nendawenjigewaad, biinish igo gakina babakewaad. 
[[eng]] Now, this was what they did: each of them had a road; now, the road of the eldest was the first to branch off towards where he was to hunt for game; and the road of the next eldest then branched off towards where he was to hunt for game; and so on, (as they stood) next in order of age, the road of each one went branching off; and now that was what they always did when they went to hunt, (the roads continued branching off) until all (the youths) had separated. 
[[ojb]] (8) Ningoding idash degoshinowaad endaawaad owaabandaanaawaa awiya gii-dagwishininid endaawaad. 
[[eng]] Now once, when they had come back home, they observed that somebody had come to the place where they lived. 
[[ojb]] (9) Weweni gii-nanaa'ichigaadeni biindig; ikwe ezhi-nametood; jiibaakwaan gaye ateni; weweni gaye gii-apishimoonike zhingobiinsa'; gaye dash misan atewan agwajiing. 
[[eng]] Nice was the arrangement (of things) inside; it was like the work of a woman; and some cooking had been done; and carefully arranged were the balsam-boughs at the sleeping-places; and there was also some fire-wood outside. 
[[ojb]] (10) Mii idash ekidod zeziikizid majiikiwis: “Skomaa waabang ningad-ab ninga-bii'aa awegwen ayaawigwen”. 
[[eng]] Accordingly said the one who was eldest: "I will simply remain at home to-morrow. I will wait to see who it can be." 
[[ojb]] (11) Geget idash weyaabaninig gii-abi majiikiwis, gaawiin dash awiiya ogii-odisigosiin. 
[[eng]] And truly on the morrow the first-born remained at home, but by nobody was he visited. 
[[ojb]] (12) Miinawaa dash weyaabaninig gakina gii-maajaawag. 
[[eng]] Therefore on the next day all went away. 
[[ojb]] (13) Mii dash miinawaa gii-dagwishinoogwen a'aw ikwe; miinawaa gii-jiibaakwe sa; gakina gaye weweni gii-biinichige, gaawiin dash imaa ayaasii. 
[[eng]] And then evidently must the woman have come again; again she must have done some cooking; and everything was nicely cleaned (in the wigwam), but she was not there. 
[[ojb]] (14) Miinawaa dash weyaabang ani-aanike-zaziikizid gii-ikido: “Skomaa ninitam ningad-ab”. 
[[eng]] So on the next day he that was the next in age said: "Just let me take a turn remaining at home." 
[[ojb]] (15) Geget weyaabaninig gii-abi gabe-giizhig, gaawiin dash gaye wiin awiiya ogii-odisigosiin. 
[[eng]] Verily, on the morrow he staid at home all day long, and by nobody was he visited. 
[[ojb]] (16) Mii go iw gaa-doodamowaad biinish igo zhaangaswi igiw oskinaweg. 
[[eng]] And that was what happened even to all the other nine youths. 
[[ojb]] (17) Mii dash a'aw nayaamawi oshiimeyimind, mii aw wiinitam ebid. 
[[eng]] And now there was the one who was their younger brother, it was now his turn to remain at home. 
[[ojb]] (18) Apii dash gakina gaa-maajaanid osayeya' mii iw gii-odisigod ikwewan; geget zazegaa-ikwewan. 
[[eng]] And when all his elder brothers had gone away, then was he visited by a woman; indeed, she was a beautiful woman. 
[[ojb]] (19) Mii dash gii-bi-onabiitaagod wiidigemigod. 
[[eng]] And so by his side she came to sit to be his wife. 
[[ojb]] (20) Apii idash degwishinowaad oskinaweg gii-gichi-minwendamoog wabamaawaad ikwewan wiidigebid oshiimeyiwaan. 
[[eng]] And when back home came the youths, they were very happy to see the woman that was a wife to their younger brother. 
[[ojb]] (21) Mii dash iniw gaa-bami'igoowaad gii-jiibaakwenid, gakina gaye odaya'iimiwaan gii-nanaa'itood a'aw ikwe. 
[[eng]] Thereupon by her were they waited upon, for them she cooked, and all their garments the woman fixed. 
[[ojb]] (22) Mii idash gaa-inaakonigewaad moozhag wiinitam ji-dagwishing aw waadiged inini; a'aw idash zeziikizid, majiikiwis, gaawiin gii-minwendanzii. 
[[eng]] And now it had been agreed among them that the one who was married would always come home first; but he who was the eldest, the first-born, did not like it. 
[[ojb]] (23) Gii-inendam: “Apegish niin wiidigemagiban!” 
[[eng]] He thought: "Would that I had been the one to marry her!" 
[[ojb]] (24) Ningoding idash gigizheb eni-maajaawaad apii gaa-ani-baked a'aw majiikiwis; gii-nibaawi megwe-zhingob gaa-waabamaad gakina gaa-bimosenid wiijikiwenya'; mii iw gii-giiwed. 
[[eng]] Now, one morning they were setting out one after another, when the first-born had left to go his way; he stood among some balsams, watching all his brothers as they went walking past; and then he went back home. 
[[ojb]] (25) Mii dash gii-gaazod besho wiigiwaaming. 
[[eng]] Thereupon he hid himself near the wigwam. 
[[ojb]] (26) Mii dash aw ikwe apii gaa-wishkwaataad biindig, mii dash bi-zaaga'ang wii-manised. 
[[eng]] And so, after the woman had finished her work indoors, she then went outside to gather some firewood. 
[[ojb]] (27) Bezhig idash mitigoon ogii-bimi-ganawaabamaan baatemisan. 
[[eng]] There was a tree which she observed had dry wood. 
[[ojb]] (28) Mii dash majiikiwis waabandang waasamowin, mii idash a'aw mitig gakina gii-bigiskised. 
[[eng]] And then the first-born beheld a flash of lightning, and at that the entire tree was splintered into pieces. 
[[ojb]] (29) Mii dash ezhi-aawadood misan a'aw ikwe. 
[[eng]] And then the woman began carrying the fire-wood. 
[[ojb]] (30) Ningoding idash animikogaabaawinid mii iwe gii-ikwoteskawaad omitigwaabiin a'aw majiikiwis, odasawaan ogii-naabisitoon odajaabiin; mii idash giimooj ezhi-naazikawaad wiinimon mii dash ezhi-bimwaad. 
[[eng]] Now, once while she had her back turned towards him, then it was that with his knee the first-born strung his bow, his feathered arrow he fixed upon the cord; thereupon slyly he went up to his sister-in-law, and then shot her. 
[[ojb]] (31) Ogii-bi-ganawaabamigoon. 
[[eng]] By her he was observed when approaching. 
[[ojb]] (32) “Baapiniziwaagan, majiikiwis, ezhichigeyan!” 
[[eng]] "What foolishness, first-born, in what you are doing!" 
[[ojb]] (33) Mii dash bijiinag gii-ani-maajaad majiikiwis. 
[[eng]] And then presently on his way went the first-born. 
[[ojb]] (34) Apii dash ba-dagwishing a'aw inini wewiidigemaaganid, gaawiin ayaasiiwan imaa endaawaad. 
[[eng]] Now, when home had come the man who had the wife, not present was she there where they lived. 
[[ojb]] (35) Mii dash gii-andawaabamaad; imaa dash endashi-maniseban iko a'aw ikwe mii imaa gii-mikawaad agaawaa bimaadizinid. 
[[eng]] Thereupon he went to look for her; now at the place where the woman was wont to gather firewood was where he found her barely yet alive. 
[[ojb]] (36) Mii dash enaad: “Awenen gaa-doodook?” 
[[eng]] Therefore he said to her: "Who did this to you?" 
[[ojb]] (37) Mii dash ekidod aw ikwe: “Mii aw gisayeniwaa zeziikizid mii aw gaa-bi-bimod zhebaa”. 
[[eng]] Whereupon said the woman: "It was that elder brother of yours, first to be born, he was the one who shot me this morning." 
[[ojb]] (38) Mii dash egod: “Nindawaa ningoji-izhiwizhishin”. 
[[eng]] So then he was told: "Please take me away somewhere." 
[[ojb]] (39) Mii dash geget gii-maajiinad. 
[[eng]] Whereupon truly he started away with her. 
[[ojb]] (40) Mii idash egod wiiwan: “Wiigiwaamens ozhitoon, mii omaa ji-ayaayaan. Baanimaa gii-midaasogonagak bi-nandawaabamishin.” 
[[eng]] And now he was told by the woman: "A small wigwam do you make, and it is there that I will stay. Not till ten days are up must you come to seek for me." 
[[ojb]] (41) Mii dash gii-giiwed a'aw inini; gii-gashkendang. 
[[eng]] Thereupon back home went the man; he felt sad about it. 
[[ojb]] (42) Apii idash gakina degoshinowaad ininiwag gaawiin owaabamaasiwaawaan iniw wiinimoowaan. 
[[eng]] And when all the men came home, they did not see their sister-in-law. 
[[ojb]] (43) Giimooj idash ogii-wiindamawaa' wiijikiwenya': “Mii gosha aw gisayenaan gaa-bimwaad”. 
[[eng]] Thereupon secretly he informed all his brothers, saying: "It was indeed our elder brother who shot her." 
[[ojb]] (44) Gaawiin dash ningod ogii-inaasiwaawaan osayeniwaan. 
[[eng]] Yet they said not a word to their elder brother. 
[[ojb]] (45) Gaye wiin dash majiikiwis gii-gashkendamokaazo. 
[[eng]] Now, the first-born made believe that he was sad too. 
[[ojb]] (46) Mii dash miinawaa wiinawaa go bamidizowaad. 
[[eng]] Thereupon once more were they waiting upon themselves. 
[[ojb]] (47) Apii dash neshwaasogonagadinig aapiji gii-inendam wii-nandawaabamad wiiwan, mii dash ezhi-izhaad. 
[[eng]] And when the eighth day came round, he became extremely anxious to see his wife, whereupon thither he went. 
[[ojb]] (48) Apii idash eni-debaabandang wiigiwaamens, mii iw owaabamaad gichi-binesiwan ani-onji-bazigwa'onid; mitigoong idash gii-booniwan idash egod: “Gidinigaa'idiz, ozaam wiiba gii-bi-nandawaabamiyan”. 
[[eng]] And when he was coming in sight of the little wigwam, he then saw a large bird rising from the place and flying away. And when it alighted on a tree, he was then addressed by it saying: "You are to be pitied, for too soon have you come to look for me." 
[[ojb]] (49) Mii dash apane maajaanid bazigwa'onid. 
[[eng]] And then off it went flying away. 
[[ojb]] (50) Mii dash gaye wiin gii-maajaad noopinanaad apane gwayak ningaabii'anong. 
[[eng]] And he too set forth, following after it, keeping always straight towards the west. 
[[ojb]] (51) Ningoding idash wajiwing mitigoon genwaakozinid gii-akwaandawe, mii dash gagwejimaad iniw mitigoon: “Gaawiin ina jigiigwaabamad [gigii-waabamaa] a'aw bemi-noopinanag?” 
[[eng]] Now, once upon a mountain he climbed a tree that was standing high, and so he asked of that tree: "Did you not see the one that I am pursuing after?" 
[[ojb]] (52) Mii dash egod: “Mii omaa gii-bimi-boonid nistigwaaning; mii apane gwayak ningaabii'anong”. 
[[eng]] Whereupon he was told: "To this place it flew, and alighted upon my head; and then away it went straight towards the west." 
[[ojb]] (53) Mii dash miinawaa gii-maajaad, mii dash igo iw gaa-doodang gabe-giizhig, mitigoo' gagwejimaad. 
[[eng]] And so once more he started on. And now that was what he did all day long, of the trees he made inquiry. 
[[ojb]] (54) Naaningodinoong agaawaa ogii-debwaabamaan mii dash nawaj bangii waaskikaad. 
[[eng]] Sometimes he could barely get within sight of it, but that was usually when he came to a turn in the trail. 
[[ojb]] (55) Apii idash wenaagoshig ookomisan ogii-odisaan daanid, mii dash gii-biindiged. 
[[eng]] And when it was evening, he came to where his grandmother was abiding, whereupon he entered. 
[[ojb]] (56) “Aaniindi, noozhis, ezhaayan?” 
[[eng]] "Whither, my grandson, are you going?" 
[[ojb]] (57) “Niwiidigemaagan nimbimi-noopinanaa”. 
[[eng]] "Of my wife am I in pursuit." 
[[ojb]] (58) “Niyaa! nozhis, gaawiin gidaa-adimaasii. Zanagad ji-odisadiban. Mii omaa gii-nibaad biindig. Nashke waabandan miskwi!” 
[[eng]] "Ah, me! my grandson, you never will overtake her. It is hard for you to reach her (there where she has gone). Here within this very place she slept. Look, see the blood!" 
[[ojb]] (59) Mii dash geget waabandang miskwiiwininik imaa gii-nibaanid. 
[[eng]] Thereupon truly he saw that the place was bloody where she had slept. 
[[ojb]] (60) Mii dash gii-ashamigod ookomisan baata-imiinan [baatemiinan] bimide gaye dagonigaadeni. 
[[eng]] Thereupon he was fed by his grandmother upon dried blueberries and upon grease mixed with them. 
[[ojb]] (61) Mii dash gii-nibaad. 
[[eng]] And then he went to sleep. 
[[ojb]] (62) Wayaabandang idash miinawaa ogii-ashamigoon ookomisan. 
[[eng]] And in the morning he was again fed by his grandmother. 
[[ojb]] (63) Mii dash miinawaa gii-maajaad, pane go gwayak ezhaad. 
[[eng]] Thereupon again he started on, always straight ahead he kept going. 
[[ojb]] (64) Mii dash miinawaa gabe-giizhig gagwejimaad mitigoon. 
[[eng]] And so again all day long he kept inquiring of the trees. 
[[ojb]] (65) Naaningodinong, “Besho gii-bimi-izhaa,” odigoon. 
[[eng]] Sometimes, "Close by she came when she passed," he was told. 
[[ojb]] (66) Naaningodinong, “Agaawaa gii-bimi-debinaagozi,” ikidowan. 
[[eng]] Sometimes, "Hardly could she be seen when she was passing," they would say. 
[[ojb]] (67) Mii miinawaa iw ezhi-washkikaad. 
[[eng]] And then again he turned off the trail. 
[[ojb]] (68) Apii dash miinawaa wenaagoshig miinawaa ookomisan ogii-odisaan. 
[[eng]] And when it was evening again, to another grandmother of his he came. 
[[ojb]] (69) “Aandi, noozhis, ezhaayan?” 
[[eng]] "Whither, my grandson, are you going?" 
[[ojb]] (70) Ogii-wiindamaawaan idash noopinanaad owiidigemaaganan. 
[[eng]] Thereupon he told her that he was in pursuit of his wife. 
[[ojb]] (71) Mii dash egod: “Niyaa! noozhis, gaawiin gidaa-odisaasii”. 
[[eng]] Whereupon he was told: "Ah, me! my grandson, you will never come to where she is." 
[[ojb]] (72) Mii dash miinawaa gii-gabaatod akikoonsing bezhigominag manoomin. 
[[eng]] Thereupon next she boiled one grain of rice in her tiny kettle. 
[[ojb]] (73) Apii idash gaa-gizhideg manoomin obi-inaako'amaagoon akikoonsan. 
[[eng]] And when the rice was done cooking, he was handed the tiny kettle with a stick. 
[[ojb]] (74) “Noozhis, wiisinin”. 
[[eng]] "My grandson, eat." 
[[ojb]] (75) Mii dash enendang aw inini: “Gaawiin ninda-debisiniisii, ozaam bangii mii dash nindashamig nookomis”. 
[[eng]] Whereupon then thought the man: "I shall not get enough to eat, such a small bit is my grandmother feeding me." 
[[ojb]] (76) Oninjing idash oziiginaan iw manoomin; aapiji mooskineni oninj biinish igo gii-debisinii. 
[[eng]] Then into his hand he poured the rice; ever so full was his hand, (and continued so) till he was sated with food. 
[[ojb]] (77) Mii dash gii-awi-nibaad. 
[[eng]] And then he went to sleep. 
[[ojb]] (78) Miinawaa dash gigizheb gaa-ishkwaa-ashamigod ookomisan miinawaa gii-ani-maajaa; pane go gwayak izhaad. 
[[eng]] And on the following morning, after he had been fed by his grandmother, he started on his way again; and always straight ahead he kept on going. 
[[ojb]] (79) Mii dash miinawaa endoodang, gagwejimaad mitigoo': “Gigii-waabamaa na awiiya ji-bimised?” 
[[eng]] Thereupon he did the same thing as before, he inquired of the trees: "Did you see any one flying by?" 
[[ojb]] (80) Naaningodinong odigoon mitigoon: “Mii omaa gii-booniid nistigwaaning”. 
[[eng]] Sometimes he was told by the trees: "Here on this head (of mine) it alighted." 
[[ojb]] (81) Mii go apane gwayak ezhaad. 
[[eng]] And always straight ahead he kept going. 
[[ojb]] (82) Miinawaa dash wenaagoshig ogii-odisaan akiwenziyan. 
[[eng]] And on the next evening he came to an old man. 
[[ojb]] (83) “Biindigen, noozhis,” odigoon. 
[[eng]] "Come in, my grandson!" he was told. 
[[ojb]] (84) Miinawaa dash ogii-ahamigoon mandaaminan akikoonsing. 
[[eng]] So next he was fed corn in a tiny kettle. 
[[ojb]] (85) Gaa-ishkwaa-wiisinid ogagwejimigoon omishoomisan: “Aaniindi ezhayaan, noozhis?” 
[[eng]] After he had eaten, he was asked by his grandfather: "Whither are you going, my grandson?" 
[[ojb]] (86) Mii dash enaad: “Niwiidigemaagan ninoopinanaa”. 
[[eng]] Thereupon he said to him: "Of my wife am I in pursuit." 
[[ojb]] (87) Mii dash egod: “Aanawenjigen, gaawiin gidaa-wadisaasii. Niibiwa anishinaaben obanaaji'aan”. 
[[eng]] So then he was told: "Stop looking for her, for you will never overtake her. Many people has she brought to destruction." 
[[ojb]] (88) Mii dash ekidod majiikiwisens: “Niwii-izhaa sa go”. 
[[eng]] Whereupon said the youth: "I am determined to go." 
[[ojb]] (89) Odigoon omishoomisan: “Miinawaa bezhig gimishoomis giga-odisaa onaagoshig, mii idash a'a weweni ge-wiindamoog ezhiwebak ezhaawan”. 
[[eng]] He was told by his grandfather: "To another grandfather of yours will you come this evening, and he will be the one to tell you rightly about the place where you are going." 
[[ojb]] (90) Mii dash gii-maajaad miinawaa; mii iw apane ezhichiged, gagwejimaad mitigoon. 
[[eng]] Thereupon he started on again; and he did what he had been continually doing, he kept on asking the trees. 
[[ojb]] (91) Miinawaa dash wenaanoshig ogii-odisaan omishoomisan; miinawaa dash ogii-ashamigoon wiiyaas bimide gaye. 
[[eng]] And on the next evening he came to his grandfather; and next he was fed upon meat and grease. 
[[ojb]] (92) Mii dash gii-nibaad. 
[[eng]] Thereupon he went to bed. 
[[ojb]] (93) Gigizheb idash ogii-ganoonigoon omishomisan: “Naawakweg giga-oditaan giishkaabikaag; mii dash imaa ji-waabandaman okanan minik imaa neboowaad anishinaabeg”. 
[[eng]] And in the morning he was addressed by his grandfather saying: "At noon you will come to a steep cliff; and there you will see the bones of all the people that have died there." 
[[ojb]] (94) Akiwenzii idash gii-andonige omashkimodaang, mii dash imaa gaa-ondinang biiwaabikoon, ozaawaabinoon; niiwin dash ogii-miinigoon; waagaabikadoon niiwin, mii dash iniw gaa-maajitood. 
[[eng]] Then the old man sought for something in his bag, and then he took out from it some metal, some pieces of copper. Now, four was he given; bent into the form of a hook were the four. And these were what he took along. 
[[ojb]] (95) Mii dash gii-oditang giishkaabikaa, mii dash imaa gii-waabandang niibiwa okanan. 
[[eng]] And when he was come at the steep cliff, he then saw there many bones. 
[[ojb]] (96) Mii dash gii-odaapinang niizh biiwaabikoon. 
[[eng]] Thereupon he took two metal pieces. 
[[ojb]] (97) “Aaniin ge-doodamaan onowe?” 
[[eng]] "What am I to do with these?" 
[[ojb]] (98) Mii dash gii-gojitood asiniing, mii dash gii-badakisenig, miinawaa dash bezhig ogii-apagidoon; mii dash maajaad giishkaabikaang akwaandawed. 
[[eng]] And when he tried them on the rock, they then stuck where they hit; thereupon with another he struck (against the rock); and so on up the cliff he climbed. 
[[ojb]] (99) Apii dash waasa eyaad, zhigwa azhiwaasinini iw biiwaabik, gaawiin badakisesinoon; ogii-webinaan. 
[[eng]] Now, when he was far (up), then dull became the (point of the) metal, it did not stick (into the rock); he flung it away. 
[[ojb]] (100) Bezhig idash miinawaa ogii-odaapinaan. 
[[eng]] So another he took. 
[[ojb]] (101) Miinawaa bezhig ogii-webinaan, miinawaa dash bezhig ogii-odaapinaan. 
[[eng]] Another he flung away, and another he took. 
[[ojb]] (102) Mii dash miinawaa maajaad. 
[[eng]] And then again he started on. 
[[ojb]] (103) Apii idash miinawaa ezhiwaasininig gaawiin badakisesinoon. 
[[eng]] And when again it became dull, it did not stick (into the rock). 
[[ojb]] (104) E! mii dash ezhi-agoojing. 
[[eng]] Alas! so there on high was he hanging. 
[[ojb]] (105) “Debwegobaniin nangwana nimishoomis gaa-ikidoban”. 
[[eng]] "Verily, the truth my grandfather told in what he said." 
[[ojb]] (106) Mii dash gii-naanaagadawendang mii idash gii-mikwenimaad memengwaan gii-bawaanaaban megwaa oskinawewid. 
[[eng]] Thereupon he recalled to mind (what had been told him in a dream), and so thought of a butterfly about which he had dreamed during the time of his youth. 
[[ojb]] (107) Mii dash ekidod: “Daga, memengwaang ningad-izhinaagoz”. 
[[eng]] Accordingly he said: "Now, like a butterfly will I look." 
[[ojb]] (108) Mii dash geget memengwaang ezhinaagozid. 
[[eng]] Whereupon truly like a butterfly he appeared. 
[[ojb]] (109) Gaawiin dash aapiji ogaskitoosin ishpiming ji-izhaad. 
[[eng]] But not so very high was he able to go. 
[[ojb]] (110) Mii dash gii-boonii aw memengwa waakoning. 
[[eng]] Thereupon the butterfly alighted upon some black lichen. 
[[ojb]] (111) Miinawaa dash gii-ikido: “Daga, zhiishiibing ningad-izhinaagoz”. 
[[eng]] So then again he said: "Well, now like a duck will I look." 
[[ojb]] (112) Geget idash zhiishiibing gii-izhinaagozi. 
[[eng]] And truly like a duck he looked. 
[[ojb]] (113) Mii idash gii-bazigwa'od mii dash enwed: “Kwenh, kwenh, kwenh, kwenh”. 
[[eng]] Thereupon, as up it flew, it quacked: "Kwenh, kwenh, kwenh, kwenh!" 
[[ojb]] (114) Mii dash gii-gashki'od ogidaabik gii-izhaad. 
[[eng]] Thereupon he succeeded in getting to the top of the mountain. 
[[ojb]] (115) Bangii go eni-maajaad ogii-mikaan giishkaabikaang. 
[[eng]] But a short way he went, when he discovered an abyss. 
[[ojb]] (116) Ogii-waabandaan idash asin mookomaaning ezhinaagoding. 
[[eng]] And he saw a rock that had the form of (the blade of) a knife. 
[[ojb]] (117) Gaawiin dash ogaskitoon imaa ji-bimosed. 
[[eng]] He was not able to walk by that way. 
[[ojb]] (118) Gegaapii idash miinawaa gii-ikido: “Daga, ajidamoong ningad-izhinaagoz”. 
[[eng]] So at last again he said: "Now like a squirrel am I going to look." 
[[ojb]] (119) Mii dash geget ajidamoong izhinaagozid. 
[[eng]] Whereupon truly like a squirrel he looked. 
[[ojb]] (120) Mii idash ajidamoo ezhi-maajiibatood. 
[[eng]] And then the squirrel started off on a run. 
[[ojb]] (121) Bekish noondaagozi, “Sank, sank, sank, sank!” inwe. 
[[eng]] At the same time it could be heard with the sound, "Sank, sank, sank, sank!" (such) was the sound it made. 
[[ojb]] (122) Apii dash gaa-dagwishing niisaaki miinawaa gii-maajaa gwayak ezhaaban. 
[[eng]] So when he was come at the foot of the mountain, he started again straight on to where he was going. 
[[ojb]] (123) Ningoding onaagoshininig mii iw waabandang odena, wiigiwaamens idash owaabandaan imaa iskwe-odena. 
[[eng]] Now, it was once on an evening that he beheld a town, and a small wigwam he saw there at the end of the town. 
[[ojb]] (124) Gaye dash owaabamaan mitigoon badakizonid naawaya'ii odenaang, gikiwe’onaatig. 
[[eng]] And he also saw a pole standing in the centre of the town, a flag-pole. 
[[ojb]] (125) Gii-biindige dash imaa wigiwaamensing, mindimooyenyan imaa daawan. 
[[eng]] And so he went into the little wigwam, (and he beheld) an old woman dwelling there. 
[[ojb]] (126) “Noozhis, biindigen!” odigoon. 
[[eng]] "My grandson, come in!" he was told. 
[[ojb]] (127) Mii dash ekidonid: “Waabang wii-gichi-ataadim ogimaa odaanisan wii-wiidigewan. Awegwen ge-bakinaagegwen mii iw ge-wiidigemaad iniw ogimaa odaanisan. Ayaangwaamizin, noozhis, gaye giin giga-nandomigoo”. 
[[eng]] And this she said: "To-morrow there is to be a great contest, for the chief's daughter is to be married. Whoever shall win in the contest will be the one to marry the chief's daughter. Do as well as you can, my grandson, for you will also be invited." 
[[ojb]] (128) Geget idash wayaabang gii-bi-nandomaa gaye wiin a'aw inini, gakina gaye odenaang eyaawaad oskinaweg gii-nandomaawag. 
[[eng]] So truly on the morrow they came to invite the man, likewise all the youths of the town were invited. 
[[ojb]] (129) Mii dash waabamaad esan, miskwesan. 
[[eng]] And so he saw a mussel-shell, a red mussel-shell. 
[[ojb]] (130) Mii dash ekidod a'aw ogimaa: “A'aw es daa-daanginaa dash biinjaya'ii; awegwen idash ged-agokenigwen oninjiing mii aw ge-wiidigemaad nindaanisan”. 
[[eng]] Thereupon said the chief: "This mussel-shell is to be touched on the inside; now, on whosoever's hand it shall stick, he shall be the one to marry my daughter." 
[[ojb]] (131) Niibawa anishinaabeg gii-biindigewag, anooj gaye binesiwag. 
[[eng]] Many people went inside, likewise all the various kinds of birds. 
[[ojb]] (132) Mii idash gii-maajiitaad aw es; gakina dash ogii-goji'aawaan ji-agokenid, gaawiin dash awiiya gii-agokesiiwan. 
[[eng]] Thereupon the mussel-shell started on its course; and every one had a chance to make it stick, but on no one did it stay. 
[[ojb]] (133) Wiin idash a'aw majiikiwisens, “Daga gaawiin awiya daa-wii-agokesiiwan oninjiing!” inendang. 
[[eng]] And as for the lad himself, "I wish it would not stick to any one's hand!" he thus thought. 
[[ojb]] (134) Biinish igo gegaa gakina odaanagiitaanginaawaan iniwe esan, gaawiin dash gii-agokesiiwan. 
[[eng]] And so it went, till nearly all had touched the shell, but without success, for it did not stick (to any one). 
[[ojb]] (135) Apii idash besho ba-ayaanid gii-inendam a'aw majiikiwisens: “Indashkaa namekwaan! ningii-bawaadaanaaban”. 
[[eng]] Now, when it was coming near, the lad thought: "If only now I had some glue! I dreamed of it (once) in the past." 
[[ojb]] (136) Mii dash geget namekwaan gii-ayaanig imaa oninjiing. 
[[eng]] It was true that some glue happened there upon his hand. 
[[ojb]] (137) Apii dash ba-bagidinimind iniw esan mi iw gii-daanginaad biinjaya'ii, mii dash gii-agokenid imaa oninjiing. 
[[eng]] And when they came, placing before him the shell, he accordingly touched it on the inside, and then it stuck there to his hand. 
[[ojb]] (138) “E!” gichi-biibaagiwag. 
[[eng]] "Hurrah!" with a great shout they cried. 
[[ojb]] (139) “E'e, ogimaa odaanisan da-wiidigewan!” 
[[eng]] "Hurrah! for the chiefs daughter is to be married." 
[[ojb]] (140) Mii dash gii-gichi-wiikonding, niibiwa bemaadizijig gii-wiikomaawag. 
[[eng]] And so there was a great time extending invitations to the feast. Many beings were asked. 
[[ojb]] (141) Wiinimoo' gaye zhaangaswi, wiiwan dash mii awe midaachiwaad; wiitaa' gaye midaachiwan. 
[[eng]] His sisters-in-law were nine in number, so therefore his wives were ten; and his brothers-in-law were also ten. 1 1 Meaning rather that the women might all be his wives if he wanted them. 
[[ojb]] (142) Mii dash imaa gii-ayaad a'aw inini. 
[[eng]] And so there at the place continued the man. 
[[ojb]] (143) Ningoding idash oganoonigoon ozhisheyan: “Na'aangish, giishpin zhiigadendaman giidaa-babaamose”. 
[[eng]] Now, once he was addressed by his father-in-law saying: "Son-in-law, if you become weary of the place, you should go off on a walk." 
[[ojb]] (144) Mii dash geget gii-maajaad mizhawashkode, ogii-waabandaan idash imaa mookijiwanibiig. 
[[eng]] Thereupon truly he went away, (and came) to a great plain, and he saw a place where the water came forth (like a fountain) from the ground. 
[[ojb]] (145) Mii dash imaa biite waabandang miskwaanig; ogii-odaapinaan idash odaasing idash ogii-atoon. 
[[eng]] And now he saw a foam there that was red; he took some, and upon his leggings he put it. 
[[ojb]] (146) Niizh ogii-mikaanan mookijiwanibiigoon; mii go miinawaa iw daa-doodang i'iw biite odaasing gii-atood. 
[[eng]] He found two fountains of water; and he did again what he had done before, he put some foam upon his leggings. 
[[ojb]] (147) Mii dash gii-ani-giiwed endaawaad. 
[[eng]] Thereupon he went his homeward way. 
[[ojb]] (148) Apii dash wayaabamigod wiiwan ozhibii'igaadenig odaasan, gii-moojigizi a'aw ikwe. 
[[eng]] Now, when he was observed by his wife with his leggings marked in design, joyful was the woman. 
[[ojb]] (149) Odinaan ogiin oosan gaye: “Niizhin makwag gii-mikawaawag,” ikido aw ikwe. 
[[eng]] She said to her mother and her father: "Two bears have been found," said the woman. 
[[ojb]] (150) A'aw idash inini gii-agaji. 
[[eng]] And the man was embarrassed. 
[[ojb]] (151) “Gaawiin ningii-mikawaasiig makwag”. 
[[eng]] "I did not find any bears." 
[[ojb]] (152) “Geget gosha gigii-mikawaawag makwag. Nashke we gidaas ezhinaagwak! Biite gosha!” ikido. 
[[eng]] "Truly, indeed, you did find some bears. Just glance at your leggings (and see) how they look! Why, there's froth!" she said. 
[[ojb]] (153) Bezhig idash wiitaan biizhaawan wawaabamigog, mii dash egod: “Nashke niitaa! geget makwag gigii-mikawaawag”. 
[[eng]] Now, one of his brothers-in-law came, and by him was he examined. Thereupon he was told: "Look, my brother-in-law! truly some bears have you seen." 
[[ojb]] (154) Mii dash ekidowaad: “Waabang isa gigad-izhaamin ji-naazikawaagwa makwag”. 
[[eng]] And then they said: "To-morrow, then, will we go get the bears." 
[[ojb]] (155) Mii idash wayaabaninig gii-maajaawaad. 
[[eng]] So then on the morrow they set out. 
[[ojb]] (156) “Aaniindi gii-waabandaman?” inaa aw inini. 
[[eng]] "Where did you see them?" was said to the man. 
[[ojb]] (157) Mii dash gikinoo'amaaged. 
[[eng]] Thereupon he pointed out the place. 
[[ojb]] (158) Apii dash gaa-waabandamowaad, gii-ikidowag: “Geget makwa omaa ayaa”. 
[[eng]] And when they had seen the place, they said: "Truly, a bear stays here." 
[[ojb]] (159) Bangii idash bikwadinaa imaa jiigaya'ii mookijiwanibiigong, mii sa imaa ayaad aw makwa. 
[[eng]] Now, there was a hillock near by the place of the fountain, and that was where the bear was. 
[[ojb]] (160) Bezhig idash netaa-noondagozid ogii-anoonaawaan ji-zegitood iwe bikwadinaans. 
[[eng]] Now, the one that was good at sounding the voice was chosen to frighten the hillock. 
[[ojb]] (161) Mii dash geget gii-bi-zaagiji-mooshkamod a'aw makwa. 
[[eng]] It was true that from out of the water into view came the bear. 
[[ojb]] (162) Wiinawaa dash imaa gaa-niibawiwaad ogii-bakitewaawaan gii-nisaawaad. 
[[eng]] And they who were standing at the place struck the bear with a blow that killed it. 
[[ojb]] (163) Aaniin dash ogii-giiwewinaawaan iniw makwan, aaniin idash geyaabi gii-izhaawag bezhig mookijiwanibiig; mii idash miinawaa bezhig makwan imaa gaa-ondinaawaad. 
[[eng]] Now, part of them came home bringing the bear, and the rest went over to where the other fountain was playing; therefore another bear they got from that place. 
[[ojb]] (164) Mii dash gaye wiinawaa gii-ani-giiwewinaawaad. 
[[eng]] And likewise they went their homeward way, taking it along. 
[[ojb]] (165) Mii dash igo iw moozhag gaa-doodang a'aw inini, gii-nandawaabandang mookijiwanibiigoon; niibawa makwan ogii-nisaawaan; nibiwa miijin ogii-ayaanaawaa ezhinitaaged a'aw inini. 
[[eng]] And so that was what the man was always doing, he went seeking for places where the water gushed out from the ground; many bears were slain; much food they had from what the man was killing. 
[[ojb]] (166) Ningoding idash gii-dibaajimo aw majiikiwisens: “Nisayenyag ayaawag owidi gaa-ba-onjiyaan; zhaangachiwag. Ganabaj gashkendamoog”. 
[[eng]] Now, once the lad got to telling about things: "There are elder brothers of mine abiding over there from whence I came; they are nine. Perhaps they are lonesome." 
[[ojb]] (167) Mii idash egod ozhishenyan: “Aaniish, giishpin wii-giiweyan gidaa-giiwe. Ogowe dash ginimoog gidayiyani wiijiiwaawag”. 
[[eng]] Thereupon was he told by his father-in-law: "Well, if you long to return home, you may go. And these your sisters-in-law may go along." 
[[ojb]] (168) Mii idash wayaabaninig gii-maajaawaad, bakaan idash gii-ani-izhaawag. 
[[eng]] And so on the following day they set out, and by a different way they went. 
[[ojb]] (169) Gaawiin imaa osheyaabikaanig gii-izhaasiiwag. 
[[eng]] Not by yonder abyss did they go. 
[[ojb]] (170) Mii go baanimaa gii-zaagaabika'amowaad mii dash gaa-izhi-onabiwaad igiw ikwewag. 
[[eng]] And then after a while, when they came out upon the edge of the cliff, then down sat the women. 
[[ojb]] (171) Gii-namadabiwaad giishkaabikaang, mii idash egod wiiwan: “Omaa nimbikwanaang ayaan. Bijiinag igo waabamiyan zhiibinikeniyaan mii imaa ogiji-aya'ii bagizon. Weweni minjimiin”. 
[[eng]] While they sat by the edge of the steep cliff, he was told by his wife: "Here at my back do you take your place. The moment you see me spread forth my arms, then upon me spring. Hold on tight to me." 
[[ojb]] (172) Mii dash geget apii zhaabinikeninid wiiwan imaa gaa-bagizod, weweni gii-minjimii. 
[[eng]] Thereupon truly, when his wife spread forth her arms, then there he flung himself, tight held he on. 
[[ojb]] (173) Mii idash gakina gaa-izhi-bimisewaad. 
[[eng]] Thereupon afterward all of them went flying away. 
[[ojb]] (174) Besho dash endaawaad a'aw majiikiwisens mii imaa gii-ani-booniiwaad. 
[[eng]] Now, near the home of the lad (and his brothers) was the place where they alighted. 
[[ojb]] (175) Mii dash miinawaa anishinaabeng gii-izhinaagoziwaad. 
[[eng]] Thereupon like people again they looked. 
[[ojb]] (176) “Mii omaa ayaayog,” odinaa'; “akawe ningad-izhaa”. 
[[eng]] "Here in this place do you remain," he said to them; "wait till I first go on ahead." 
[[ojb]] (177) Mii dash ani-izhaad, anooj awesiiya' bimikawewa'. 
[[eng]] And as he went on his way, (he saw) where the tracks of all kinds of game were passing. 
[[ojb]] (178) Apii idash weditang endaawaad owaabandaan nengaw zaagijisininig iskwaandeng. 
[[eng]] And when he reached the place where (his elder brothers) lived, he saw sand coming forth from the doorway. 
[[ojb]] (179) Apii idash eni-oditang endaawaad, oganoonaa' osayenya': “Nisayeyidog! Nindagwishin”. 
[[eng]] And when he reached the place where they lived, he addressed his elder brothers, saying: "O my elder brothers! I have now come home." 
[[ojb]] (180) Majiikiwis idash onawadinaan odemikwaan gwaaba'onga'ige skwandeng. 
[[eng]] Then the first-born took up a spoon (and) dipped up sand at the doorway. 
[[ojb]] (181) Mii idash miinawaa ganoonigod oshiimeyan: “Geget, nisayeyidog, nindagwishin”. 
[[eng]] Thereupon another time was he addressed by his younger brother saying: "Truly, my elder brothers, I have come home." 
[[ojb]] (182) Mii dash majiikiwis inaabid, odoo(n)gibidoonan oskinzhigoon, mii dash geget waabamaad oshiimeyan. 
[[eng]] And when the first-born looked, he opened his eyes with his hand, whereupon he truly beheld his little brother. 
[[ojb]] (183) Mii dash debibinaad gii-ojiimaad. 
[[eng]] And when he had seized him, he kissed him. 
[[ojb]] (184) Mii dash egod: “Giziibiigiiyog, weweni gaye biizikonayeyog. Binaakweyog”. 
[[eng]] Thereupon he was told: "Bathe yourselves, and clothe yourselves neatly in fine raiment. Comb your hair." 
[[ojb]] (185) Mii dash gaa-iskwaa-zazegaawaad gii-naazikawaad wiinimoo'. 
[[eng]] And after they were all gaily dressed, he went after his sisters-in-law. 
[[ojb]] (186) Mii dash enaad: “Nimbikwanaang byeyaayog [bi-ayaayog]. Byaakawishig [bi-akawishig], mii dash igo ji-ani-onabiitawegwaa nisayeyag”. 
[[eng]] Thereupon he said to them: "Behind me come. Keep at my back, and in a regular order are you to take your seats beside my elder brothers." 
[[ojb]] (187) Apii dash bibaandigewaad [bi-biindigewaad] a'aw gaa-wiidiged gii-onabiwag. 
[[eng]] And when hither they came entering in, then the man who was married sat down along with the rest. 
[[ojb]] (188) Mii dash igo bebezhig gii-eni-onabiitawaawaad iniw ininiwa' igiw ikwewag. 
[[eng]] Thereupon the women sat down with the men, each beside a man. 
[[ojb]] (189) Aapiji idash wiin skwaaj gii-onabiitawaa a'aw majiikiwis zaziikizid. 
[[eng]] And the very last to have one sit beside him was the first-born, oldest in years. 
[[ojb]] (190) Apii dash gaa-onabininid iniw ikwewan, mii iw gii-odaapinang obagamaagan, mii dash agwajiing gii-izhaad gegoo omamaadweganaandoon. 
[[eng]] And after the woman was seated t then he took up his war-club, whereupon out of doors he went, (and) he was heard beating upon something. 
[[ojb]] (191) Mii nangwana iw makwan. 
[[eng]] It happened to be a bear. 
[[ojb]] (192) Mii dash gaa-izhi-jiibaakwewaad maamawi igiw ikwewag, anooj gaye gegoo, ogii-giizhizaanaawaa; mii dash maamawi gii-wiisiniwaad. 
[[eng]] And after the women had joined together in the task of cooking the food, then all sorts of things they cooked; and then all ate together. 
[[ojb]] (193) Mii idash imaa ginwenzh gii-ayaawaad. 
[[eng]] Thereupon at that place they continued for a long while. 
[[ojb]] (194) Mii sa iw binewidis gii-agoode. 
[[eng]] And so the gizzard of the ruffed grouse now hangs aloft.
