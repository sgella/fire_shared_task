#CURRENT URL http://www.peeral.com/index.php/component/content/archive%3Fyear%3D2008%26month%3D10
[[fuf]] Daartol jaŋde Fulfulde to Ejipt
[[eng]] Created: Tuesday, 28 October 2008 19:08 Author: Marju Jalloh 
[[fuf]] Ko adii fof ngannden won'de Keer tesketee ko jaŋngirde yaltinoore annduɓe fellitɓe gollan'de Fulfulde, yooɓiiɓe munyal e laawol sattungol, ɗuum fof ko ngam ɓamtude gootal e ɗemɗe e pine Afrik ɓurɗe mawnude. Yottaade e ngal baawal, wonaano ko we... Daartol cosegol Kawtal e Tabital Pulaaku Winndere (3)
[[eng]] Created: Sunday, 26 October 2008 11:27 Author: Tijjaani Mbaalo 
[[fuf]] Ko sakkitoode callalal binndanɗe sincegol KJPF e TPW woni ko Peeral addani on ɗoo hannde… Timmitingol yiilirde mayre e cuɓagol goomuuji golle Tabital Pulaaku Winndere tonngaaɗe he goomuuji nayi ɗiin ngoni : Goomu ɗemngal. Goomu pinal. Goomu jokko... Ciimtol koolol 2ɓol Tabital Pulaaku, ñalnde 15 & 16 lewru juko (uksatu), to Briksel, Beljik (1).
[[eng]] Created: Saturday, 25 October 2008 01:00 Author: Saajo Bah
[[fuf]] Ko wano no holliranoo non gila law, wonde ɗee ñalaaɗe ɗiɗi ko sabboranooɗe wa juldaandu, seteeɓe fuɗɗino arude gila 11 muuɗum, hiɓe jaɓɓee fii ngoo welowelo jogorngo waɗude hakkunde Fulɓe immortooɓe e leyɗe seertuɗe.Meɗen etoo ɗoo jubb... Sarlon: Heertiindi: Jeewtidal e Hooreejo Nunɗal e Yeesoojo PPRC saaluɗo on
[[eng]] Created: Tuesday, 21 October 2008 21:16 Author: Marju Jalloh
[[fuf]] Kɗl: Alhajji Jallo, Firiton.Pooɗondiro fii hooreyaagal leñol Fulɓe ngal ka Nokkuure Hiirnaangeere
[[eng]] (Freetown)
[[fuf]] towii, ɓaawo nde Alhajji Almaami Baaba Alii faatinoo. Fewndo Baaba Alii faatii ñalnde 17 Morse
[[eng]] (July)
[[fuf]] e ndee ɗoo hitaande, hari fop hino de... Bresil: Fukungal
[[eng]] (football)
[[fuf]] naatii ka futu (musée)
[[eng]] Created: Tuesday, 21 October 2008 21:04 Author: Marju Jalloh
[[fuf]] Wano no anndirɗen non Bresil ko leydi ɓurndi leyɗe fow waawude ko yowitii e latugol mullere, ko ɗum waɗi ɓe udditi e nder saare Saw Pawlo futu tawa ko fii fukungal (fuku-koyngal) tun wernetee ɗon.Hino anndaa fukungal, ko lamɗo e nder Bresil, sab... Kumpa Lamndiiɗo! Yiidugol Peeral e Ndane Seelenke Jallo, binndiyanke nantuɗo innde e nder leydi Gine…(2)
[[eng]] Created: Wednesday, 15 October 2008 15:44 Author: Saajo Bah
[[fuf]] Ko ɗoo jeewtidal (interview) Peeral e o ceerno timmi.Peeral: E nder ko dogoton kon fii janngingol pulaar, no gasa hara caɗeele hino toon ɗe tiindotoɗon maaɗum hara newaare hino ton weeɓinoore muraadu ndun, ko honɗum woni e saɗtingol janngingol pul... Daartol cosegol Kawtal e Tabital Pulaaku Winndere (2)
[[eng]] Created: Wednesday, 15 October 2008 15:21 Author: Tijjaani Mbaalo
[[fuf]] Haa jooni ko e jokkitagol callalal binndanɗe yowitiiɗe e sincegol KJPF e TPW Peeral woni e siimtande on ɗoo…Nde tawnoo, ina wonoo he yilaaji yimɓe arɓe e tawraaɓe ɓee fof, sosde fedde fulɓe winndere, darantoonde janngude e jannginde ɗemngal ful... Tabital Pulaaku Beljik waylitii yiilirde mum, Ibraahiima Timmbo ardinaama!
[[eng]] Created: Friday, 10 October 2008 13:00 Author: Administrator
[[fuf]] Pellet hino famɗi ñila hannde mo nanaali fii Tabital Pulaaku Winndere/International, si tawii ko o hittinɗo fii Fulfulde, Pulaaku walla Afrikyankaagal. Tabital Pulaaku ko kawtital ngal terɗeyaagal mum saakitii e winndere ndee fuu; gila Afrika, Orop, A... Kumpa Lamndiiɗo! Yiidugol Peeral e Ndane Seelenke Jallo, binndiyanke nantuɗo innde e nder leydi Gine…(1)
[[eng]] Created: Saturday, 04 October 2008 19:23 Author: Saajo Bah
[[fuf]] Hannde ñalnde 14 -09-2008 ɗoo e Antwerpen, Peeral hino yiidude e mawɗ men tedduɗo toppitiiɗo janngingol fulfulde e wallifaade defte mum, o sifanoo en no golle ɗen jokkiraa toon e nder leydi Gine e kala ka o woni gollirde e nder winndere nden. &l... Daartol cosegol Kawtal e Tabital Pulaaku Winndere(1)
[[eng]] Created: Saturday, 04 October 2008 18:13 Author: Tijjaani Mbaalo
[[fuf]] Honikooɓe Leñol, mino jubbana on seeɗa he daartol Tabital Pulaaku Winndere. Banndiraaɓe tedduɓe, eɗen nganndi he yontaaji jeytaare Afrik, hoohooɓe yiɗnooɓe yoo en keɓen ko’e men, eɓe njoginoo yiɗde yoo en keɓ jeytaare men, to senngo faggudu...
