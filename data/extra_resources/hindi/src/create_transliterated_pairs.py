import os
import re
if __name__ == '__main__':
	transliterated_pairs = {}

	chars = ['!', '.', '?', ',']

	for directory in os.listdir("../transliteration_pairs"):
		for filename in os.listdir(os.path.join("../transliteration_pairs", directory)):
			lines = open(os.path.join("../transliteration_pairs", directory, filename), 'r').readlines()
			lines = [line.strip() for line in lines if line.strip() and len(line)>5]
			print len(lines), os.path.join("../transliteration_pairs", directory, filename)
			for i in range(0, len(lines), 2):
				#print i, len(lines)
				latin = re.sub('[%s]' % ''.join(chars), '', lines[i]).replace(";","\t").strip().split()
				script = re.sub('[%s]' % ''.join(chars), '', lines[i+1]).replace(";","\t").strip().split()
				#latin = lines[i].strip().split(";")
				#script = lines[i+1].strip().split(";")
				if len(latin)!= len(script):
					nothing = "nothing"
					#print "ERROR", i, os.path.join("../transliteration_pairs", directory, filename), len(latin), len(script)
					#print lines[i], latin
					#print lines[i+1], script
				else:
					for latin_word, script_word in zip(latin, script):
						if latin_word == "" and script_word == "":
							pass
						elif latin_word =="" or script_word =="":
							print "ERROR", latin ,script							
						if not transliterated_pairs.has_key(latin_word):
							transliterated_pairs[latin_word] = []
						#if not transliterated_pairs[latin_word].has_key(script_word):
						#	transliterated_pairs[latin_word][script_word] = 0
						transliterated_pairs[latin_word].append(script_word)
	fp = open("../transliteration_pairs2.txt", 'w')
	for latin_word in transliterated_pairs:
		#print latin_word, transliterated_pairs[latin_word]
		fp.write(latin_word+"\t"+transliterated_pairs[latin_word][0]+"\n")
	fp.close()
						
				

